﻿using Domain.Domain;
using Domain.Interface;
using Hangfire;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OCRTool.Controllers
{
    [Authorize]
    public class TemplateController : Controller
    {
        // GET: Template
        ITemplateRepository TemplateRepository = new TemplateRepository();
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        public ActionResult Index()
        {
            return View();
        }
        
        [CustomAuthorize("!STAFF")]
        public ActionResult AddEdit(int templateId = 0)
        {
            ViewBag.templateId = templateId;
            return View();
        }

        [CustomAuthorize("!STAFF")]
        public ActionResult AddTemplate(Template t, TemplateDetails[] tdArr)
        {
            t.CreatedBy = User.Identity.Name;
            t.CreatedOn = DateTime.Now;

            var systemLog = new SystemLog
            {
                User = User.Identity.Name,
                DTLog = DateTime.Now,
                Type = "TEMPLATE"
            };

            if (t.Id > 0)
            {
                systemLog.Description = $"The user updated the template {t.TemplateName} (" +
                    $"{t.Id}).";
            }
            else
            {
                systemLog.Description = $"The user added a new template {t.TemplateName}.";
            }

            var result = TemplateRepository.AddUpdateTemplate(t, tdArr);

            systemLog.Status = result.Status == "SUCCESS" ? "COMPLETED" : "FAILED";

            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize("!STAFF")]
        public ActionResult ChangeTemplateStatus(int templateId, string templateName, bool willDisable)
        {
            var systemLog = new SystemLog
            {
                User = User.Identity.Name,
                DTLog = DateTime.Now,
                Type = "TEMPLATE",
                Description = $"The user {(willDisable == true ? "disabled" : "enabled")} the " +
                $"{templateName} template."
            };

            var result = TemplateRepository.ChangeTemplateStatus(templateId, willDisable);

            systemLog.Status = result == "SUCCESS" ? "COMPLETED" : "FAILED";

            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize("!STAFF")]
        public ActionResult SaveTemplateFile(HttpPostedFileBase FileUpload, string templateId)
        {
            var result = "";

            try
            {
                var splitfilename = FileUpload.FileName.Split('.');
                var fileExtension = splitfilename[splitfilename.Length - 1];

                FileUpload.SaveAs(Server.MapPath("~/Template Files/" + templateId + "." + fileExtension));

                result = "SUCCESS";
            }
            catch (Exception ex)
            {
                result = ex.Message;
            }

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTemplateList(bool includeDisabled = false)
        {
            return Json(TemplateRepository.GetTemplateList(includeDisabled), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTemplateInfo(int templateId) {
            TemplateInfo ti = new TemplateInfo {
                Template = TemplateRepository.GetTemplateById(templateId),
                TemplateDetails = TemplateRepository.GetTemplateDetails(templateId)
            };

            return Json(ti, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetTemplateDetails(int templateId)
        {
            return Json(TemplateRepository.GetTemplateDetails(templateId), JsonRequestBehavior.AllowGet);
        }

        public class TemplateInfo {
            public Template Template { get; set; }
            public List<TemplateDetails> TemplateDetails { get; set; }
        }
    }
}