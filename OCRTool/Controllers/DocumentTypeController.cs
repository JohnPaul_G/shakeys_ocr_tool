﻿using Domain.Domain;
using Domain.Interface;
using Hangfire;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OCRTool.Controllers
{
    [Authorize]
    public class DocumentTypeController : Controller
    {
        IDocumentTypeRepository DocumentTypeRepository = new DocumentTypeRepository();
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        // GET: DocumentType
        public ActionResult Index()
        {
            return View();
        }

        [CustomAuthorize("!STAFF")]
        public ActionResult AddUpdateDocumentType(DocumentType dt)
        {
            var systemLog = new SystemLog
            {
                User = User.Identity.Name,
                DTLog = DateTime.Now,
                Type = "DOCUMENT TYPE"
            };

            if (dt.Id > 0)
            {
                systemLog.Description = $"The user updated the document type {dt.DocumentTypeName} (" +
                    $"{dt.Id}).";
            }
            else
            {
                systemLog.Description = $"The user added a new document type {dt.DocumentTypeName}.";
            }

            var result = DocumentTypeRepository.AddUpdate(dt);

            systemLog.Status = result == "SUCCESS" ? "COMPLETED" : "FAILED";

            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDocumentTypeList()
        {
            var result = DocumentTypeRepository.GetDocumentTypeList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}