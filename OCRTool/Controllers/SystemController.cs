﻿using Domain.Interface;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OCRTool.Controllers
{
    public class SystemController : Controller
    {
        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        // GET: System
        public ActionResult Logs()
        {
            return View();
        }

        public ActionResult GetSystemLogsList(DateTime? dateFrom = null, DateTime? dateTo = null)
        {
            if (dateFrom == null)
            {
                dateFrom = DateTime.Today;   
            }

            if (dateTo == null)
            {
                dateTo = DateTime.Today.AddDays(1);
            }
            else
            {
                dateTo = dateTo?.AddDays(1);
            }

            var systemLogs = SystemLogRepository.GetSystemLogs(dateFrom, dateTo);

            return Json(systemLogs, JsonRequestBehavior.AllowGet);
        }
    }
}