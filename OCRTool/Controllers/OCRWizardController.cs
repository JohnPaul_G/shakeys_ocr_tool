﻿using Domain.Domain;
using Domain.Interface;
using Google.Cloud.Vision.V1;
using Hangfire;
using Hangfire.Server;
using ImageMagick;
using Infrastructure.Repository;
using Ionic.Zip;
using Microsoft.AspNet.SignalR;
using OCRTool.Models;
using OCRTool.SignalR;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace OCRTool.Controllers
{
    [CustomAuthorize]
    public class OCRWizardController : Controller
    {
        ITemplateRepository TemplateRepository = new TemplateRepository();
        IHangfireRepository HangfireRepository = new HangfireRepository();
        MagickReadSettings settings;
        MagickImageCollection images;
        OCRResultHub OCRResultHub = new OCRResultHub();
        //int currentRunningJobCount = 0;

        public ActionResult Index() => View();

        public void MoveFiles(string from, string to)
        {
            if (System.IO.File.Exists(from))
            {
                if (System.IO.File.Exists(to))
                {
                    System.IO.File.Delete(to);
                }

                System.IO.Directory.Move(from, to);
            }
        }

        public ActionResult StartScanning(int templateId, string srConnectionId)
        {
            try
            {
                var user = User.Identity.Name;

                if (String.IsNullOrWhiteSpace(user))
                {
                    return Json("FAILED", JsonRequestBehavior.AllowGet);
                }

                string jobId = BackgroundJob.Enqueue(() => ReadScannedFilesInBackground(templateId, user, Server.MapPath("~/Users/"),
                    Server.MapPath("~/map-for-work-ext-00355262-89f13c387e5f.json"), null, srConnectionId));

                return Json(jobId, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(ex.Message, JsonRequestBehavior.AllowGet);
            }
        }

        public void DeleteBackgroundJob(string jobId = "0")
        {
            HangfireRepository.DeleteJob(jobId);
            BackgroundJob.Delete(jobId);
        }
        
        public void DeleteUserActiveJobs()
        {
            var activeJobs = HangfireRepository.GetActiveJobs(User.Identity.Name);

            foreach (var job in activeJobs)
            {
                HangfireRepository.DeleteJob(job.JobId);
                BackgroundJob.Delete(job.JobId);
            }
        }

        public void ReadScannedFilesInBackground(int templateId,
            string user,
            string serverPath,
            string serverPath2,
            PerformContext context,
            string srConnectionId
            )
        {
            try
            {
                string from = serverPath + user + "\\Scanned Files\\";
                string to = serverPath + user + "\\OCR Reading Files\\";
                string jobId = context.BackgroundJob.Id;

                string lastJobId = "";

                HangfireRepository.AddJob(jobId, user);

                bool stop = false;
                int scannedFiles = 0;
                var SRcontext = GlobalHost.ConnectionManager.GetHubContext<OCRResultHub>();

                DateTime lastHeartBeat = DateTime.Now;

                while (stop == false)
                {
                    DirectoryInfo directory = new DirectoryInfo(from);
                    FileInfo[] ScannedFilesList = directory.GetFiles("*", SearchOption.AllDirectories);

                    foreach (var file in ScannedFilesList)
                    {
                        lastHeartBeat = DateTime.Now;

                        scannedFiles++;

                        var filename = file.Name;

                        Notification notif = new Notification
                        {
                            Status = "SUCCESS",
                            Type = "SCANNED FILE",
                            Name = filename,
                            Details = scannedFiles + " scanned files",
                            Count = scannedFiles,
                            Id = jobId
                        };

                        SRcontext.Clients.Client(srConnectionId).getScanStatus(notif);

                        MoveFiles(from + file.Name, to + file.Name);

                        //this statement is to avoid applying OCR for multiple files at the same time which may would
                        // result to a system failure

                        if (lastJobId == "")
                        {
                            lastJobId = BackgroundJob.Enqueue(() => OCRReadContent(templateId, filename, user, serverPath, serverPath2, srConnectionId, jobId));
                        }
                        else
                        {
                            lastJobId = BackgroundJob.ContinueJobWith(lastJobId, () => OCRReadContent(templateId, filename, user, serverPath, serverPath2, srConnectionId, jobId));
                        }
                    };

                    var lastHeartBeatSpan = DateTime.Now.Subtract(lastHeartBeat).TotalMinutes;
                    
                    if (lastHeartBeatSpan > 30)
                    {
                        DeleteBackgroundJob(jobId);
                        stop = true;
                    }
                    else if(HangfireRepository.GetHangfireJob(jobId) == null)
                    {
                        stop = true;
                    }
                }

                var srContext = GlobalHost.ConnectionManager.GetHubContext<OCRResultHub>();

                var notif2 = new Notification
                {
                    Type = "STOPPED SCANNING",
                    Id = jobId
                };

                srContext.Clients.Client(srConnectionId).getScanStatus(notif2);
            }
            catch (Exception ex)
            {
                var srContext = GlobalHost.ConnectionManager.GetHubContext<OCRResultHub>();

                var notif = new Notification
                {
                    Type = "ERROR",
                    Message = ex.Message
                };

                srContext.Clients.Client(srConnectionId).getScanStatus(notif);
            }
        }

        public void OCRReadContent(int templateId, string filename, string user, string serverPath, string serverPath2,
            string srConnectionId, string jobId)
        {
            var returnResult = new OCRResult();
            var ocrFinalContentTmp = new List<OCRFileContent>();
            var splitFileName = filename.Split('.');
            var filetype = splitFileName[splitFileName.Length - 1].ToUpper();
            var filepath = serverPath + user + "\\OCR Reading Files\\" + filename;

            try
            {
                System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", serverPath2);

                var template = TemplateRepository.GetTemplateById(templateId);
                var setuplist = TemplateRepository.GetTemplateDetails(templateId);

                if (filetype == "PDF")
                {
                    MagickNET.SetTempDirectory("C:\\ImageMagick\\");
                    // Settings the density to 300 dpi will create an image with a better quality
                    settings = new MagickReadSettings
                    {
                        Density = new Density(300, 300)
                    };
                    images = new MagickImageCollection();
                    images.Read(filepath, settings);
                }

                foreach (var setup in setuplist)
                {
                    Rectangle cropRect = new Rectangle(setup.XCoordinate, setup.YCoordinate, setup.Width, setup.Height);

                    if (filetype == "PDF")
                    {
                        var result = "";

                        if ((setup.Page - 1) > images.Count())
                        {
                            result = "Out of page index range error.";
                        }
                        else
                        {
                            images[(setup.Page ?? 0) - 1].Resize(template.PageWidth, template.PageHeight);
                            Bitmap img = images[(setup.Page ?? 0) - 1].ToBitmap();

                            Bitmap final = img.Clone(cropRect, img.PixelFormat);

                            var imageBytes = ImageToByte(final);
                            var image = Google.Cloud.Vision.V1.Image.FromBytes(imageBytes);
                            var client = ImageAnnotatorClient.Create();
                            var response = client.DetectText(image);

                            if (response.FirstOrDefault() != null && response.FirstOrDefault().Description != null)
                            {
                                result = response.First().Description.ToString();
                            }
                        }

                        var ocrFileContent = new OCRFileContent()
                        {
                            Fieldname = setup.FieldName,
                            Text = result
                        };
                        ocrFinalContentTmp.Add(ocrFileContent);
                    }
                    else
                    {
                        Bitmap img = new Bitmap(filepath);
                        Bitmap final = img.Clone(cropRect, img.PixelFormat);

                        var imageBytes = ImageToByte(final);
                        var image = Google.Cloud.Vision.V1.Image.FromBytes(imageBytes);
                        var client = ImageAnnotatorClient.Create();
                        var response = client.DetectText(image);

                        if (response.FirstOrDefault().Description != null)
                        {
                            var ocrFileContent = new OCRFileContent()
                            {
                                Fieldname = setup.FieldName,
                                Text = response.First().Description.ToString(),
                            };
                            ocrFinalContentTmp.Add(ocrFileContent);
                        }
                    }
                }

                MoveFiles(filepath, serverPath + user + "\\OCR Read Files\\" + filename);

                returnResult.Id = jobId;
                returnResult.TemplateDetails = setuplist;
                returnResult.OCRFileContentList = ocrFinalContentTmp;
                returnResult.Filename = filename;
                returnResult.Template = template;
                returnResult.Type = "OCR RESULT";

                var context = GlobalHost.ConnectionManager.GetHubContext<OCRResultHub>();
                context.Clients.Client(srConnectionId).getScanStatus(returnResult);
            }
            catch (Exception ex)
            {
                var srContext = GlobalHost.ConnectionManager.GetHubContext<OCRResultHub>();

                var notif = new Notification
                {
                    Type = "ERROR",
                    Message = ex.Message
                };

                srContext.Clients.Client(srConnectionId).getScanStatus(notif);
            }
        }

        //public ActionResult PostFilesToDMS(string[] files)
        //{
        //    try
        //    {
               

        //        return Json("SUCCESS", JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json("FAILED", JsonRequestBehavior.AllowGet);
        //    }
        //}
        [AllowAnonymous]
        public async Task<string> PostFilesToDMS(string[] files)
        {
            string path = Server.MapPath("~\\Users\\") + User.Identity.Name + "\\";

            using (ZipFile zip = new ZipFile())
            {
                for (int i = 0; i < files.Length; i++)
                {
                    zip.AddFile(path + "OCR Read Files\\" + files[i], "");
                }

                zip.Save(path + "OCR Read Files.zip");
            };

            string actionUrl = "";
            string paramString = "";
            //Stream paramFileStream ;
            byte[] paramFileBytes = System.IO.File.ReadAllBytes(path + "OCR Read Files.zip");

            string result;

            HttpContent stringContent = new StringContent(paramString);
            //HttpContent fileStreamContent = new StreamContent(paramFileStream);
            HttpContent bytesContent = new ByteArrayContent(paramFileBytes);
            using (var client = new HttpClient())
            using (var formData = new MultipartFormDataContent())
            {
                //formData.Add(stringContent, "param1", "param1");
                /*formData.Add(fileStreamContent, "file1", "file1")*/;
                formData.Add(bytesContent, "zippedFile", "zippedFile");
                var response = await client.PostAsync(actionUrl, formData);
                if (!response.IsSuccessStatusCode)
                {
                    return null;
                }
                result = await response.Content.ReadAsStringAsync();
            }

            return result;
        }
        
        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public class Notification
        {
            public string Status { get; set; }
            public string Details { get; set; }
            public string Name { get; set; }
            public string Type { get; set; }
            public string Id { get; set; }
            public int Count { get; set; }
            public string Message { get; set; }
        }
    }
}