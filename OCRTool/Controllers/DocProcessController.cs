﻿using Domain.Domain;
using Domain.Interface;
using Hangfire;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OCRTool.Controllers
{
    public class DocProcessController : Controller
    {
        readonly IDocProcessRepository DocProcessRepository = new DocProcessRepository();

        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        public ActionResult Index()
        {
            return View();
        }

        public ActionResult GetDocProcessList() {
            return Json(DocProcessRepository.GetDocProcessList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult GetDocProcessItemsList(int docProcessId) {
            return Json(DocProcessRepository.GetDocProcessItemsList(docProcessId), JsonRequestBehavior.AllowGet);
        }

        [CustomAuthorize("!STAFF")]
        public ActionResult AddUpdateDocProcess(DocProcess dp, DocProcessItem[] dpiArr)
        {
            var systemLog = new SystemLog
            {
                User = User.Identity.Name,
                DTLog = DateTime.Now,
                Type = "DOCUMENT PROCESS"
            };

            if (dp.Id > 0)
            {
                systemLog.Description = $"The user updated the document type {dp.DocProcessName} (" +
                    $"{dp.Id}).";
            }
            else
            {
                systemLog.Description = $"The user added a new document type {dp.DocProcessName}.";
            }
            
            var result = DocProcessRepository.AddUpdateDocProcess(dp, dpiArr);
            
            systemLog.Status = result == "SUCCESS" ? "COMPLETED" : "FAILED";

            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));
            
            return Json(result, JsonRequestBehavior.AllowGet);
        }
    }
}