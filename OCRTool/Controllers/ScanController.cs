﻿using Domain.Domain;
using Domain.Interface;
using Google.Cloud.Vision.V1;
using ImageMagick;
using Infrastructure.Repository;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OCRTool.Controllers
{
    [Authorize]
    public class ScanController : Controller
    {
        ITemplateRepository TemplateRepository = new TemplateRepository();
        MagickReadSettings settings;
        MagickImageCollection images;


        // GET: Scan
        public ActionResult ScannedFiles()
        {
            return View();
        }

        public ActionResult GetScannedFiles()
        {
            //var files = Directory.GetFiles("C:\\OCR FILES\\Users\\" + User.Identity.Name +"\\");

            DirectoryInfo directory = new DirectoryInfo(Server.MapPath("~/Users/") + User.Identity.Name + "/");
            FileInfo[] FileList = directory.GetFiles("*", SearchOption.AllDirectories);
            
            return Json(FileList.Select(x => new { x.FullName, x.CreationTime}).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult OCRReadContent(int templateId, string filename)
        {
            var returnResult = new OCRResult();
            var ocrFinalContentTmp = new List<OCRFileContent>();
            var splitFileName = filename.Split('.');
            var filetype = splitFileName[splitFileName.Length - 1].ToUpper();
            var filepath = Server.MapPath("~/Users/" + User.Identity.Name + "/" + filename);
            
            try
            {
                System.Environment.SetEnvironmentVariable("GOOGLE_APPLICATION_CREDENTIALS", Server.MapPath("~/map-for-work-ext-00355262-89f13c387e5f.json"));

                var template = TemplateRepository.GetTemplateById(templateId);
                var setuplist = TemplateRepository.GetTemplateDetails(templateId);

                if (filetype == "PDF") {
                    MagickNET.SetTempDirectory("C:\\ImageMagick\\");
                    // Settings the density to 300 dpi will create an image with a better quality
                    settings = new MagickReadSettings
                    {
                        Density = new Density(300, 300)
                    };
                    images = new MagickImageCollection();
                    images.Read(filepath, settings);
                }

                foreach (var setup in setuplist)
                {
                    Rectangle cropRect = new Rectangle(setup.XCoordinate, setup.YCoordinate, setup.Width, setup.Height);

                    if (filetype == "PDF")
                    {
                        images[(setup.Page ?? 0) - 1].Resize(template.PageWidth, template.PageHeight);
                        Bitmap img = images[(setup.Page ?? 0) - 1].ToBitmap();
                        images[0].Write("C:\\Users\\dizon\\Desktop\\temp files\\test.jpg");

                        Bitmap final = img.Clone(cropRect, img.PixelFormat);

                        var imageBytes = ImageToByte(final);
                        var image = Google.Cloud.Vision.V1.Image.FromBytes(imageBytes);
                        var client = ImageAnnotatorClient.Create();
                        var response = client.DetectText(image);

                        if (response.FirstOrDefault() != null && response.FirstOrDefault().Description != null)
                        {
                            var ocrFileContent = new OCRFileContent()
                            {
                                Fieldname = setup.FieldName,
                                Text = response.First().Description.ToString()
                            };
                            ocrFinalContentTmp.Add(ocrFileContent);
                        }
                    }
                    else
                    {
                        Bitmap img = new Bitmap(filepath);
                        Bitmap final = img.Clone(cropRect, img.PixelFormat);

                        var imageBytes = ImageToByte(final);
                        var image = Google.Cloud.Vision.V1.Image.FromBytes(imageBytes);
                        var client = ImageAnnotatorClient.Create();
                        var response = client.DetectText(image);

                        if (response.FirstOrDefault().Description != null)
                        {
                            var ocrFileContent = new OCRFileContent()
                            {
                                Fieldname = setup.FieldName,
                                Text = response.First().Description.ToString()
                            };
                            ocrFinalContentTmp.Add(ocrFileContent);
                        }
                    }
                }
                returnResult.TemplateDetails = setuplist;
                returnResult.OCRFileContentList = ocrFinalContentTmp;
            }
            catch (Exception e)
            {
                returnResult.ErrorResult = e.Message;
            }

            return Json(returnResult, JsonRequestBehavior.AllowGet);
        }
        
        public static byte[] ImageToByte(System.Drawing.Image img)
        {
            using (var stream = new MemoryStream())
            {
                img.Save(stream, System.Drawing.Imaging.ImageFormat.Png);
                return stream.ToArray();
            }
        }

        public class OCRData
        {
            public string Fieldname { get; set; }
            public int Height { get; set; }
            public int Width { get; set; }
            public int XCoordinate { get; set; }
            public int YCoordinate { get; set; }
        }

        public class OCRResult
        {
            public List<OCRFileContent> OCRFileContentList { get; set; }
            public List<TemplateDetails> TemplateDetails { get; set; }
            public string ErrorResult { get; set; }
        }

        public class OCRFileContent
        {
            public string Fieldname { get; set; }
            public string Text { get; set; }
        }
    }
}