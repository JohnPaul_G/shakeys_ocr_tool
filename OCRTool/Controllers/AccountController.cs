﻿using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using OCRTool.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin.Security.OpenIdConnect;
using System.Web.Routing;
using Domain.Interface;
using Infrastructure.Repository;
using System.IO;
using Domain.Domain;
using Hangfire;

namespace OCRTool.Controllers
{
    [CustomAuthorize]
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;

        ISystemLogRepository SystemLogRepository = new SystemLogRepository();

        IUserRepository UserRepository = new UserRepository();

        // GET: Account
        [CustomAuthorize("ADMIN")]
        public ActionResult Users() => View();
        
        [AllowAnonymous]
        public ActionResult AccessDenied() => View();

        [CustomAuthorize("ADMIN")]
        public ActionResult GetUserList()
        {
            var result = UserRepository.GetUserList();

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult LoginForm(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        [AllowAnonymous]
        public ActionResult MicrosoftLogin()
        {
            if (Request.IsAuthenticated)
            {
                return RedirectToAction("Index", "Home");
            }
            else
            {
                HttpContext.GetOwinContext().Authentication.Challenge(
                    new AuthenticationProperties { RedirectUri = "http://localhost:30291/Account/ValidateMicrosoftAccount" },
                    OpenIdConnectAuthenticationDefaults.AuthenticationType);

                //TDIPS VM Testing Environment
                //HttpContext.GetOwinContext().Authentication.Challenge(
                //    new AuthenticationProperties { RedirectUri = "https://190.100.5.5:8086/Account/ValidateMicrosoftAccount" },
                //    OpenIdConnectAuthenticationDefaults.AuthenticationType);

                return null;
            }
        }


        [CustomAuthorize("ADMIN")]
        public async Task<ActionResult> DeleteUser(string username)
        {
            var systemLog = new SystemLog
            {
                DTLog = DateTime.Now,
                User = User.Identity.Name,
                Type = "ACCOUNT DELETION",
                Description = $"The user deleted {username}'s account.",
                Status = "COMPLETED"
            };

            //get User Data from Userid
            var user = await UserManager.FindByNameAsync(username);

            //List Logins associated with user
            var logins = user.Logins;

            //Gets list of Roles associated with current user
            var rolesForUser = await UserManager.GetRolesAsync(user.Id);

            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(store);

            foreach (var login in logins.ToList())
            {
                await UserManager.RemoveLoginAsync(login.UserId, new UserLoginInfo(login.LoginProvider, login.ProviderKey));
            }

            if (rolesForUser.Count() > 0)
            {
                foreach (var item in rolesForUser.ToList())
                {
                    var result = await UserManager.RemoveFromRoleAsync(user.Id, item);
                }
            }

            await UserManager.DeleteAsync(user);

            var userDir = Server.MapPath("/Users/") + username;

            if (System.IO.Directory.Exists(userDir))
            {
                Directory.Delete(userDir, true);
            }

            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

            return Json("SUCCESS", JsonRequestBehavior.AllowGet);
        }

        [AllowAnonymous]
        public ActionResult ValidateMicrosoftAccount()
        {
            if (Request.IsAuthenticated)
            {
                var userClaims = User.Identity as System.Security.Claims.ClaimsIdentity;

                var claims = userClaims.Claims.ToArray();

                var email = "";

                for (int i = 0; i < claims.Length; i++)
                {
                    if (claims[i].ToString().Contains("preferred_username"))
                    {
                        email = claims[i].ToString().Split(' ')[1];
                    }
                }

                var systemLog = new SystemLog
                {
                    User = "UNKNOWN",
                    DTLog = DateTime.Now,
                    Type = "ACCOUNT LOGIN",
                    Description = "An user tried to login to the system using the Microsoft account - " +
                        email + "."
                };

                var user = UserManager.FindByEmail(email);
                
                if (user == null)
                {
                    HttpContext.GetOwinContext().Authentication.SignOut(
                    CookieAuthenticationDefaults.AuthenticationType);
                    AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);

                    systemLog.Status = "FAILED";
                    BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                    return RedirectToAction("AccessDenied", "Account");
                }
                else
                {
                    HttpContext.GetOwinContext().Authentication.SignOut(
                    CookieAuthenticationDefaults.AuthenticationType);

                    SignInManager.SignIn(user, true, true);

                    systemLog.Status = "COMPLETED";
                    BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return RedirectToAction("LoginForm", "Account");
            }
        }

        [CustomAuthorize]
        public ActionResult Logout()
        {
            var systemLog = new SystemLog
            {
                User = User.Identity.Name ?? "UNKNOWN",
                DTLog = DateTime.Now,
                Type = "ACCOUNT LOGIN",
                Description = "The user logged out.",
                Status = "COMPLETED"
            };

            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            HttpContext.GetOwinContext().Authentication.SignOut(
            CookieAuthenticationDefaults.AuthenticationType);
            return RedirectToAction("LoginForm", "Account");
        }

        public ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            private set
            {
                _signInManager = value;
            }
        }

        [CustomAuthorize("ADMIN")]
        public ActionResult AddUser(Models.User model)
        {
            try
            {
                if (!ModelState.IsValid)
                {
                    string errmsg = "";

                    foreach (ModelState modelState in ViewData.ModelState.Values)
                    {
                        foreach (ModelError error in modelState.Errors)
                        {
                            errmsg += error.ErrorMessage + ". ";
                        }
                    }
                    return Json(errmsg, JsonRequestBehavior.AllowGet);
                }

                var systemLog = new SystemLog
                {
                    User = User.Identity.Name,
                    DTLog = DateTime.Now,
                    Type = "ACCOUNT CREATION",
                    Description = $"The user added a new account ({model.Username})"
                };

                var userManager = new UserManager<ApplicationUser>(new UserStore<ApplicationUser>(new ApplicationDbContext()));

                var user = new ApplicationUser
                {
                    UserName = model.Username,
                    Email = model.Email,
                    DepartmentId = model.DepartmentId,
                    Fullname = model.Fullname
                };

                var result = userManager.Create(user, model.Password);
                if (result.Succeeded)
                {
                    //userManager.AddToRole(userManager.FindByName(user.UserName).Id, model.Role);
                    System.IO.Directory.CreateDirectory(Server.MapPath("/Users/") + model.Username + "/Scanned Files/");
                    System.IO.Directory.CreateDirectory(Server.MapPath("/Users/") + model.Username + "/OCR Read Files/");
                    System.IO.Directory.CreateDirectory(Server.MapPath("/Users/") + model.Username + "/OCR Reading Files/");

                    userManager.AddToRole(userManager.FindByName(user.UserName).Id, model.UserType);

                    systemLog.Status = "COMPLETED";
                    BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                    return Json("SUCCESS", JsonRequestBehavior.AllowGet);
                }
                else
                {
                    systemLog.Status = "FAILED";
                    BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                    return Json(result.Errors, JsonRequestBehavior.AllowGet);
                }
            }
            catch (Exception ex)
            {
                return Json("FAILED", JsonRequestBehavior.AllowGet);
            }
        }

        [CustomAuthorize("ADMIN")]
        public ActionResult EditUser(EditUser model)
        {
            if (!ModelState.IsValid)
            {
                string errmsg = "";

                foreach (ModelState modelState in ViewData.ModelState.Values)
                {
                    foreach (ModelError error in modelState.Errors)
                    {
                        errmsg += error.ErrorMessage + ". ";
                    }
                }
                return Json(errmsg, JsonRequestBehavior.AllowGet);
            }

            var systemLog = new SystemLog
            {
                User = User.Identity.Name,
                DTLog = DateTime.Now,
                Type = "ACCOUNT UPDATE",
                Description = $"The user updated {model.Username}'s account details."
            };


            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(store);
            var editUser = manager.FindByName(model.Username);
            
            editUser.Fullname = model.Fullname;
            editUser.DepartmentId = model.DepartmentId;
            
            var result = manager.Update(editUser);

            if (result.Succeeded)
            {
                //userManager.AddToRole(userManager.FindByName(user.UserName).Id, model.Role);

                systemLog.Status = "COMPLETED";
                BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                var oldRole = manager.GetRoles(editUser.Id).FirstOrDefault();

                if (oldRole != null) {
                    result = manager.RemoveFromRole(manager.FindByName(editUser.UserName).Id, oldRole);
                }
                
                manager.AddToRole(manager.FindByName(editUser.UserName).Id, model.UserType);

                return Json("SUCCESS", JsonRequestBehavior.AllowGet);
            }
            else
            {
                systemLog.Status = "FAILED";
                BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                return Json(result.Errors, JsonRequestBehavior.AllowGet);
            }
        }

        [Authorize]
        public ActionResult GetCurrentUserDetails()
        {
            var store = new UserStore<ApplicationUser>(new ApplicationDbContext());
            var manager = new UserManager<ApplicationUser>(store);
            var user = manager.FindByName(User.Identity.Name);
            
            return Json(user, JsonRequestBehavior.AllowGet);
        }
        
        [AllowAnonymous]
        public ActionResult LoginUser(Models.User model, string returnUrl)
        {
            var systemLog = new SystemLog
            {
                User = "UNKNOWN",
                DTLog = DateTime.Now,
                Type = "ACCOUNT LOGIN",
                Description = "An user tried to login using the system credential - " + model.Username
            };

            var result = SignInManager.PasswordSignIn(model.Username, model.Password, false, shouldLockout: false);
            switch (result)
            {
                case SignInStatus.Success:
                    systemLog.Status = "COMPLETED";
                    BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                    if (Url.IsLocalUrl(returnUrl))
                    {
                        return Json(returnUrl, JsonRequestBehavior.AllowGet);
                    }
                    else {
                        return Json(Url.Action("Index", "Home"), JsonRequestBehavior.AllowGet);
                    }
                default:
                    systemLog.Status = "FAILED";
                    BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

                    return Json("INVALID", JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult ChangePassword(ChangePassword model)
        {
            var systemLog = new SystemLog
            {
                User = User.Identity.Name,
                DTLog = DateTime.Now,
                Type = "CHANGE PASSWORD",
                Description = "The user changed his/her password."
            };

            var result = UserManager.ChangePassword(User.Identity.GetUserId(), model.OldPassword, model.NewPassword);

            if (result.Succeeded)
            {
                systemLog.Status = "COMPLETED";
                BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));
                return Json("SUCCESS", JsonRequestBehavior.AllowGet);
            }

            systemLog.Status = "FAILED";
            BackgroundJob.Enqueue(() => SystemLogRepository.AddSystemLog(systemLog));

            AddErrors(result);

            return Json(result, JsonRequestBehavior.AllowGet);
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        public ActionResult Logoff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Login", "Account");
        }

        private IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }
    }
}

public class CustomAuthorize : AuthorizeAttribute
{
    private readonly string[] allowedroles;

    public CustomAuthorize(params string[] roles)
    {
        this.allowedroles = roles;
    }

    protected override bool AuthorizeCore(HttpContextBase httpContext)
    {
        bool authorize = false;

        var user = httpContext.User;

        bool isNotEqualExisting = false;

        for (var i = 0; i < allowedroles.Length; i++)
        {
            if (allowedroles[0].Contains("!"))
            {
                isNotEqualExisting = true;
            }
        }

        if (allowedroles.Length == 0 && user.Identity.IsAuthenticated)
        {
            authorize = true;
        }

        if (isNotEqualExisting == true)
        {
            for (var i = 0; i < allowedroles.Length; i++)
            {
                if (httpContext.User.IsInRole(allowedroles[i].Substring(1)))
                {
                    return false;
                }
            }

            return true;
        }

        foreach (var role in allowedroles)
        {
            if (httpContext.User.IsInRole(role))
            {
                authorize = true;
            }
        }

        return authorize;
    }

    protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
    {
        if (filterContext.HttpContext.Request.IsAuthenticated)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                filterContext.Result = new HttpUnauthorizedResult();
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                    RouteValueDictionary(new { controller = "Account/AccessDenied" }));
            }
        }
        else
        {
            filterContext.Result = new RedirectToRouteResult(
            new RouteValueDictionary {
                { "action", "LoginForm" },
                { "controller", "Account" },
                { "returnUrl", "%2F"}
            }
            );
        }
    }
}