var directoryList;
var selectedUser;
$.ajax({
    async: false,
    url: "https://dms.shakeys.solutions/api/integration/gtdmstrfldrs/5437264954385422313",
    success: function (data) {
        directoryList = data;
        var departments = FilterJson(directoryList, "ParentId", null);
        $("#txtDepartment").append(GenerateOptions(departments, "Id", "Foldername", "Select Department"));
        SetSelect2();
    }
});
$(document).ready(function () {
    $("#btnAddUserModal").click(function () {
        selectedUser = "";
        $("#modalUser input").val("");
        $("#modalUser select").val("");
        SetSelect2();
        $("#txtPassword, #txtPassword2,#txtUsername, #txtEmail")
            .closest(".form-group")
            .css("display", "block");
        $("#modalUser .modal-title strong").html("Add User");
        $("#modalUser").modal();
    });
    $("#btnSaveUser").click(function () {
        var username = $("#txtUsername").val();
        var email = $("#txtEmail").val();
        var password = $("#txtPassword").val();
        var password2 = $("#txtPassword2").val();
        var usertype = $("#txtUserType").val();
        var department = $("#txtDepartment").val();
        var fullname = $("#txtFullname").val();
        if (selectedUser == "") {
            if (username == '' || email == '' || password == '' || password2 == '' || usertype == '' || department == '' || fullname == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please fill in all of the inputs!'
                });
                return false;
            }
            if (password != password2) {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Repeat Password doesn\'t match!'
                });
                return false;
            }
            else {
                HoldOn.open({
                    theme: "sk-circle",
                    message: "Adding the user.."
                });
                $.ajax({
                    url: "/Account/AddUser",
                    dataType: "json",
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    data: JSON.stringify({
                        model: {
                            Username: username,
                            Email: email,
                            Password: password,
                            UserType: usertype,
                            DepartmentId: department,
                            Fullname: fullname
                        }
                    }),
                    success: function (data) {
                        HoldOn.close();
                        if (data == "SUCCESS") {
                            Swal.fire({
                                icon: 'success',
                                title: 'Successful',
                                text: 'New user is created successfully!'
                            }).then(function () {
                                location.reload();
                            });
                        }
                        else {
                            Swal.fire({
                                icon: 'error',
                                title: 'Failed',
                                text: data
                            });
                        }
                    }
                });
            }
        }
        //for user update
        else {
            if (usertype == '' || department == '' || fullname == '') {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'Please fill in all of the inputs!'
                });
                return false;
            }
            HoldOn.open({
                theme: "sk-circle",
                message: "Updating the user.."
            });
            $.ajax({
                url: "/Account/EditUser",
                dataType: "json",
                type: "POST",
                contentType: "application/json; charset=utf-8",
                data: JSON.stringify({
                    model: {
                        Username: selectedUser,
                        UserType: usertype,
                        DepartmentId: department,
                        Fullname: fullname
                    }
                }),
                success: function (data) {
                    HoldOn.close();
                    if (data == "SUCCESS") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Successful',
                            text: 'The user is updated successfully!'
                        }).then(function () {
                            location.reload();
                        });
                    }
                    else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Failed',
                            text: data
                        });
                    }
                }
            });
        }
    });
    $("#dtUser").DataTable({
        ajax: {
            url: "/Account/GetUserList",
            dataType: "json",
            dataSrc: ""
        },
        columns: [
            {
                data: null,
                title: "Department",
                render: function (data) {
                    var department = GetRowFromJsonByValue(directoryList, "Id", data.DepartmentId);
                    if (department.length == 0) {
                        return "Unknown Department";
                    }
                    else {
                        return department.Foldername;
                    }
                }
            },
            { data: "Fullname", title: "Full Name" },
            { data: "Username", title: "Username" },
            { data: "Email", title: "Email" },
            {
                data: null,
                title: "User Type",
                render: function (data) {
                    return "<div style='text-transform:capitalize;'>" + data.UserType.toLowerCase() + "</div>";
                }
            },
            {
                data: null,
                title: "Action",
                render: function (data) {
                    var details = "<td><button onclick='EditUser(" + ParamParse(data) + ")' class=\"btn btn-sm btn-primary btn-square mr-1\">\n                                <span class=\"fa fa-edit\"></span> \n                                Edit\n                                </button>\n                                <button onclick=\"DeleteUser('" + data.Username + "')\" class=\"btn btn-sm btn-danger btn-square mt-lg-0 mt-sm-2\">\n                                <span class=\"fa fa-ban\">\n                                </span> \n                                Delete\n                                </button></td>";
                    return details;
                }
            },
        ],
        columnDefs: [
            {
                targets: [0, 1, 2, 3, 4],
                "class": "text-left"
            },
            {
                targets: [5],
                "class": "text-right"
            }
        ],
        initComplete: function () {
            $("#dtUser").setCustomSearch({
                exemption: [5]
            });
        }
    });
});
function SetSelect2() {
    $("select").select2({
        width: '100%',
        dropdownParent: $("#modalUser")
    });
}
function DeleteUser(username) {
    Swal.fire({
        title: 'Are you sure?',
        text: "Are you sure that you want to delete the user account? \n            Once deleted, the user won't be no longer to access his/her account!",
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then(function (result) {
        if (result.value) {
            HoldOn.open({
                message: "Deleting ...",
                theme: "sk-circle"
            });
            $.ajax({
                url: "/Account/DeleteUser?username=" + username,
                success: function (data) {
                    HoldOn.close();
                    if (data == "SUCCESS") {
                        Swal.fire({
                            text: "The user account is deleted" +
                                " successfully.",
                            icon: "success",
                            title: "Success"
                        }).then(function () {
                            window.location.reload();
                        });
                    }
                    else {
                        Swal.fire({
                            text: "There is an error processing your request.",
                            icon: "error",
                            title: "Oops.."
                        });
                    }
                }
            });
        }
    });
}
function EditUser(data) {
    selectedUser = data.Username;
    $("#modalUser .modal-title strong").html("Edit User");
    $("#txtFullname").val(data.Fullname);
    $("#txtUserType").val(data.UserType);
    $("#txtDepartment").val(data.DepartmentId);
    $("#txtPassword, #txtPassword2, #txtUsername, #txtEmail")
        .closest(".form-group")
        .css("display", "none");
    SetSelect2();
    $("#modalUser").modal();
}
function FilterJson(json, filterField, filterValue) {
    var result = [];
    $.each(json, function (i, item) {
        if (item[filterField] == filterValue) {
            result.push(item);
        }
    });
    return result;
}
function GenerateOptions(json, valueField, nameField, placeholder) {
    placeholder = placeholder || "Select";
    var options = "<option disabled selected value=''>" + placeholder + "</option>";
    $.each(json, function (i, item) {
        options += "<option value='" + item[valueField] + "'>" + item[nameField] + "</option>";
    });
    return options;
}
function GetRowFromJsonByValue(json, field, value) {
    var result = [];
    $.each(json, function (i, item) {
        if (item[field] == value) {
            result = item;
        }
    });
    return result;
}
//# sourceMappingURL=user.js.map