﻿var currentFileExtension;

var currentFilename = "";

var currentTemplate;

var currentFile;

const colors = [
    {
        Name: "Red",
        Code: "#FF0000"
    },
    {
        Name: "Blue",
        Code: "#0000FF"
    },
    {
        Name: "Purple",
        Code: "#800080"
    },
    {
        Name: "Yellow",
        Code: "#FFFF00"
    },
    {
        Name: "Cyan",
        Code: "#00FFFF"
    },
    {
        Name: "Pink",
        Code: "#FF00FF"
    },
    {
        Name: "Brown",
        Code: "#A52A2A"
    },
    {
        Name: "Black",
        Code: "#000000"
    },
    {
        Name: "Maroon",
        Code: "#800000"
    },
    {
        Name: "Gray",
        Code: "#808080"
    },
    {
        Name: "Green",
        Code: "#008000"
    },
    {
        Name: "Orange",
        Code: "#FFA500"
    },
]

var pageDetails = {
    totalPages: 0,
    currentPage: 1,
}

$(document).ready(function () {
    var dtTemplate = $("#dtTemplate").DataTable({
        ajax: {
            url: "/Template/GetTemplateList?includeDisabled=true",
            dataType: "json",
            dataSrc: ""
        },
        columns: [
            {
                data: null,
                title: "Document Type",
                render: function (data) {
                    let details = data.DocumentType.DocumentTypeName
                    return details;
                }
            },
            { data: "TemplateName", title: "Template" },
            { data: "CreatedBy", title: "Created By" },
            {
                data: null,
                title: "Created On",
                render: function (data) {
                    let details = FormatDate(data.CreatedOn, "MM/dd/yyyy")
                    return details;
                }
            },
            {
                data: null,
                title: "Status",
                render: function (data) {
                    let details;

                    if (data.IsDisabled == true) {
                        details = "<span class='kt-badge kt-badge--danger kt-badge--inline'>Disabled</span>"
                    } else
                    {
                        details = "<span class='kt-badge kt-badge--success kt-badge--inline'>Enabled</span>"
                    }


                    return details;
                }
            },
            {
                data: null,
                title: "Action",
                render: function (data) {
                    let href = "/Template/AddEdit?templateId="

                    let details = `<button onclick='ViewTemplate(${ParamParse(data)})' 
                                class='btn btn-sm btn-primary btn-square mr-1'>
                                <span class='fa fa-eye'></span> 
                                View
                                </button>`

                    if (!isStaff)
                    {
                        details += `<button onclick=\"window.location.href='${href + data.Id}'\" 
                                class='btn btn-sm btn-primary btn-square mr-1 mt-lg-0 mt-sm-2'>
                                <span class='fa fa-edit'></span> 
                                Edit
                                </button>
                                <button onclick=\"ChangeTemplateStatus(${data.Id},
                                '${data.TemplateName.replace(/'/g, "")}',
                                ${!data.IsDisabled})\"
                                ${(data.IsDisabled == true ? "class='btn btn-sm btn-success btn-square mt-lg-0 mt-sm-2'>" :
                                "class='btn btn-sm btn-danger btn-square mt-lg-0 mt-sm-2'>")}
                                ${(data.IsDisabled == true ? "<span class='fa fa-check'>" :
                                "<span class='fa fa-ban'>")}
                                </span> 
                                ${(data.IsDisabled == true ? "Activate" : "Deactivate")}
                                </button>`
                    }
                                
                    return details;
                }
            }
        ]
    })

    $("#btnNext").click(function () {
        if (pageDetails.currentPage < pageDetails.totalPages) {
            pageDetails.currentPage += 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })

    $("#btnPrev").click(function () {
        if (pageDetails.currentPage > 1) {
            pageDetails.currentPage -= 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })
})

function ViewTemplate(template) {
    $("#modalTemplate .modal-header strong").html(template.TemplateName);
    
    $.ajax({
        url: "/Template/GetTemplateDetails?templateId=" + template.Id,
        success: function (data) {
            currentTemplate = data;
            
            currentFileExtension = template.FileExtension

            if (currentFileExtension.toUpperCase() == "PDF") {
                currentFileExtension = "pdf"
            } else {
                currentFileExtension = "IMG";
            }

            $("#file-area").html("<canvas id='canvas-file'></canvas>");

            SetImgOnCanvas("/Template Files/" + template.Id + "." + currentFileExtension, RedrawRectangle);

            $("#modalTemplate").modal();
        }
    })
}

function ChangeTemplateStatus(templateId, templateName, willDisable)
{
    willDisable = willDisable || false

    let msg = "";

    if (willDisable == true) {
        msg = "Are you sure that you want to deactivate the template now? The users won't be able to " +
            "use the template until it is activated again."
    } else {
        msg = "Are you sure that you want to activate the template? Once activated, the users would be " +
            "able to use the template again."
    }

    Swal.fire({
        title: 'Are you sure?',
        text: msg,
        icon: 'warning',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, '+ (willDisable ? "deactivate" : "activate") +' it!'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: "/Template/ChangeTemplateStatus?templateId=" + templateId +
                    "&templateName=" + templateName +
                    "&willDisable=" + willDisable,
                success: function (data) {
                    if (data == "SUCCESS") {
                        Swal.fire({
                            text: "The template is " + (willDisable ? "disabled" : "activated") +
                                " successfully.",
                            icon: "success",
                            title: "Success"
                        }).then(function () {
                            window.location.reload();
                        })
                    } else {
                        Swal.fire({
                            text: "There is an error processing your request.",
                            icon: "error",
                            title: "Oops.."
                        })
                    }
                }
            })
        }
    })
}

function LoadPDFAsImage(onRender) {
    $("#page-config-area").removeClass("d-none").addClass("d-flex");
    onRender = onRender || function () { };

    currentFile.promise.then(function (pdf) {
        pageDetails.totalPages = pdf.numPages;
        pdf.getPage(pageDetails.currentPage).then(function (page) {
            var scale = 1;
            var viewport = page.getViewport({ scale: scale, });
            //
            // Prepare canvas using PDF page dimensions
            //
            var canvas = document.getElementById('canvas-file');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            //
            // Render PDF page into canvas context
            //
            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };

            var renderTask = page.render(renderContext);

            renderTask.promise.then(function () {
                onRender();
            })

            $("#lblPageDetails").html(pageDetails.currentPage + " of " + pageDetails.totalPages);
        });
    });
}

function RedrawRectangle(options) {
    options = options || {};
    options.exemption = options.exemption || [];

    var canvasContext = document.getElementById('canvas-file').getContext('2d');

    $("#legends").html("");

    let indexFilled = 0;

    $.each(currentTemplate, function (i, item) {
        if (options.exemption.indexOf(item.ordinal) == - 1 && item.XCoordinate > 0) {
            DrawRectangleOnCanvas(canvasContext, item.Height, item.Width, item.XCoordinate, item.YCoordinate, item.Page, colors[indexFilled].Code)

            $("#legends").append(`<div class='mr-3 mb-2'><span style='color:${colors[indexFilled].Code}' class='fa fa-square'></span> 
            ${item.FieldName} (${colors[indexFilled].Name})</div>`)

            indexFilled++;
        }
    });
}

function DrawRectangleOnCanvas(canvasContext, height, width, x, y, p, color) { //p -> page
    color = color || "red"

    if (pageDetails.currentPage == p) { //Draw rectangle only on current page
        height = parseInt(height);
        width = parseInt(width);
        x = parseInt(x);
        y = parseInt(y);

        canvasContext.beginPath();
        canvasContext.rect(x, y, width, height);
        canvasContext.strokeStyle = color;
        canvasContext.lineWidth = 1.7;
        canvasContext.stroke();
    }
}

function SetImgOnCanvas(src, onRender) {
    onRender = onRender || function () { };

    if (currentFileExtension == "pdf") {
        currentFile = pdfjsLib.getDocument(src);

        pageDetails.currentPage = 1;
        LoadPDFAsImage(onRender);
    } else {
        var canvasContext = document.getElementById('canvas-file').getContext('2d');
        currentFile = new Image();
        currentFile.src = src;

        currentFile.onload = function () {
            canvasContext.canvas.height = currentFile.height;
            canvasContext.canvas.width = currentFile.width;
            canvasDetails.height = currentFile.height;
            canvasDetails.width = currentFile.width;
            
            canvasContext.drawImage(currentFile, 0, 0);

            onRender();
        }
    }
}

