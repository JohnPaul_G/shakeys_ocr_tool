﻿var filename;
var currentFile;
var currentFileType;
var currentFileSrc;
var currentCoordinates = [];

var templateLst;

//for draggable files
var beingDragged
var draggedFrom;

var pageDetails = {
    totalPages: 0,
    currentPage: 1
}

$.ajax({
    url: "/Template/GetTemplateList",
    success: function (data) {
        console.log(data);
    }
})


$(document).ready(function () {
    $.ajax({
        url: "/DocProcess/GetDocProcessList",
        success: function (data) {
            $.each(data, function (i, item) {
                $("#cbxProcess").append("<option value='" + item.Id + "'>" + item.DocProcessName + "</option>")
            })

            $('#cbxProcess').select2({
                width: '100%'
            })
        }
    })

    $.ajax({
        url: "/Scan/GetScannedFiles",
        success: function (data) {
            $.each(data, function (i, item) {
                let filepath = item.FullName.split("\\");

                $("#files_container .content").append("<div onclick='ViewFile(this)' filename='" +
                    filepath[filepath.length - 1] + "' class='file'>" +
                    filepath[filepath.length - 1] + "</div>")
            })
        }
    })

    $("#cbxProcess").change(function () {
        $("#process_summary ul").html("");
        $("#required_list_container .content").html("");

        $.ajax({
            url: "/DocProcess/GetDocProcessItemsList?docProcessId=" + $(this).val(),
            success: function (data) {
                $.each(data, function (i, item) {
                    $("#process_summary ul").append("<li>" + item.DocumentType.DocumentTypeName +
                        " (" + item.MinQty + ")</li>")

                    $("#required_list_container .content").append("<div class='required-docs'>" +
                        item.DocumentType.DocumentTypeName + " (" + item.MinQty + ")</div>");
                })

                InitializeDraggableFile();
            }
        })
    })


    $('#smartwizard').smartWizard({
            selected: 0,  // Initial selected step, 0 = first step 
            keyNavigation:true, // Enable/Disable keyboard navigation(left and right keys are used if enabled)
            //autoAdjustHeight:true, // Automatically adjust content height
            //cycleSteps: false, // Allows to cycle the navigation of steps
            backButtonSupport: true, // Enable the back button support
            useURLhash: true, // Enable selection of the step based on url hash
            lang: {  // Language variables
                next: 'Next', 
                previous: 'Previous'
            },         
            disabledSteps: [],    // Array Steps disabled
            errorSteps: [],    // Highlight step with errors
            theme: 'default',
            transitionEffect: 'fade', // Effect on navigation, none/slide/fade
            transitionSpeed: '400'
      });

    var dtFiles = $("#dtFiles").DataTable({
        ajax: {
            url: "/Scan/GetScannedFiles",
            type: "GET",
            dataSrc: '',
            dataType: "json"
        },
        columns: [
            {
                data: null,
                title: "File",
                render: function (data) {
                    let filepath = data.FullName.split("\\");
                    return filepath[filepath.length - 1];
                }
            },
            {
                data: null,
                title: "Creation Time",
                render: function (data) {
                    let details = FormatDate(data.CreationTime, "MM/dd/yyyy hh:mm")
                    return details;
                }
            },
            {
                data: null,
                title: "Action",
                render: function (data, type, row) {
                    let filepath = data.FullName.split("\\");

                    let details = "<button filename='" + filepath[filepath.length - 1] +
                        "' onclick='ViewFile(this)' class='btn btn-sm btn-primary btnViewFile'>View</button> ";

                    return details;
                }
            }
        ],
    })

    $.ajax({
        url: "/Template/GetTemplateList",
        success: function (data) {
            if (data) {
                $.each(data, function (i, item) {
                    $("#cbxTemplate").append("<option value='" + item.Id +
                        "'>"+ item.TemplateName +"</option>")
                })
            }
        }
    })

    $("#btnApply").click(function () {
        let template = $("#cbxTemplate").val();

        if (!template) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please choose a template first!'
            })

            return false;
        }
        
        HoldOn.open({
            theme: "sk-circle",
            message: "Reading file ..."
        })

        $.ajax({
            url: "/Scan/OCRReadContent?templateId=" + template + "&filename=" + filename,
            success: function (data) {
                if (!data.OCRFileContentList.length) {
                    currentCoordinates = [];

                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'There is no read text result!'
                    })
                } else {
                    $("#results_container").html("");
                    
                    $.each(data.OCRFileContentList, function (i, item) {
                        $("#results_container").append("<div class='mt-3'>" +
                            "<label class='lblfield'>" + item.Fieldname + " : </label>" +
                            "</div>" +
                            "<div>" +
                            "<div class='form-control' style='height:unset;'>"
                            + item.Text + "</div>" +
                            "</div>")
                    });

                    var canvasContext = document.getElementById('myCanvas').getContext('2d');

                    currentCoordinates = data.TemplateDetails;

                    $.each(currentCoordinates, function (i, val) {
                        DrawRectangleOnCanvas(canvasContext, val.Height, val.Width, val.XCoordinate, val.YCoordinate, val.Page, val.FieldName);
                    });

                    HoldOn.close();
                }
            }
        })
    })

    $("#btnNext").click(function () {
        if (pageDetails.currentPage < pageDetails.totalPages) {
            pageDetails.currentPage += 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })

    $("#btnPrev").click(function () {
        if (pageDetails.currentPage > 1) {
            pageDetails.currentPage -= 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })
})

function InitializeDraggableFile() {
    $(".file").draggable({
        revert: true,
        start: function (event, ui) {
            ViewFile(ui.helper);
            beingDragged = ui.helper;
        }
    });

    $("#files_container .content").droppable({
        drop: function (event, ui) {
            dropped = $(this);
            beingDragged.appendTo($(this));
        }
    });

    $(".required-docs").droppable({
        drop: function (event, ui) {
            dropped = $(this);
            beingDragged.appendTo($(this));

            beingDragged.wrap("<div class='test'></div>");
            let parent = beingDragged.closest(".test");
            parent.append("<select class='form-control form-control-sm'><option>Please choose a template</option></select>");
        }
    });
}

function ViewFile(that) {
    $("#page-config-area").removeClass("d-flex").addClass("d-none");
    let userdir = $("#username").attr("username");
    $("#results_container").html("");
    currentCoordinates = [];

    filename = $(that).attr("filename");
    currentFileType = filename.split(".");
    currentFileSrc = "/users/" + userdir + "/" + filename;
    currentFileType = currentFileType[currentFileType.length - 1].toLowerCase();
    
    $("#file-area").html("<canvas id='myCanvas'></canvas>");
    
    if (currentFileType == "pdf") {
        currentFile = pdfjsLib.getDocument(currentFileSrc);
        pageDetails.currentPage = 1;
        LoadPDFAsImage();
    } else {
        var canvasContext = document.getElementById('myCanvas').getContext('2d');
        currentFile = new Image();
        currentFile.src = currentFileSrc;
        currentFile.onload = function () {
            canvasContext.canvas.height = currentFile.height;
            canvasContext.canvas.width = currentFile.width;

            canvasContext.drawImage(currentFile, 0, 0);
        }
    }

    $(".modal-title").html(filename);
    $("#modalFile").modal();
}

function LoadPDFAsImage(onRender) {
    $("#page-config-area").removeClass("d-none").addClass("d-flex");
    onRender = onRender || function () { };

    currentFile.promise.then(function (pdf) {
        pageDetails.totalPages = pdf.numPages;
        pdf.getPage(pageDetails.currentPage).then(function (page) {
            var scale = 1;
            var viewport = page.getViewport({ scale: scale, });
            //
            // Prepare canvas using PDF page dimensions
            //
            var canvas = document.getElementById('myCanvas');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;

            //
            // Render PDF page into canvas context
            //
            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };

            var renderTask = page.render(renderContext);

            renderTask.promise.then(function () {
                onRender();
            })

            $("#lblPageDetails").html(pageDetails.currentPage + " of " + pageDetails.totalPages);
        });
    });
}

function DrawRectangleOnCanvas(canvasContext, height, width, x, y, p, field) { //p -> page
    console.log(p);

    if (pageDetails.currentPage == p) { //Draw rectangle only on current page
        height = parseInt(height);
        width = parseInt(width);
        x = parseInt(x);
        y = parseInt(y);

        canvasContext.beginPath();
        canvasContext.rect(x, y, width, height);
        canvasContext.strokeStyle = "green";
        canvasContext.stroke();
        canvasContext.font = "9pt sans-serif";
        canvasContext.fillStyle = "red";
        canvasContext.fillText(field, (x + width) + 3, y + ((height / 2) + 3))
    }
}

function RedrawRectangle() {
    if (currentCoordinates.length > 0) {
        var canvasContext = document.getElementById('myCanvas').getContext('2d');

        $.each(currentCoordinates, function (i, val) {
            DrawRectangleOnCanvas(canvasContext, val.Height, val.Width, val.XCoordinate, val.YCoordinate, val.Page, val.FieldName)
        });
    }
}