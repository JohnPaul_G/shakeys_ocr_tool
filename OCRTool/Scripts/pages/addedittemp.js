﻿var jcrop_api;
var currentFile;
var currentFileType;
var currentFileExtension;
var fileData = "";

//would be populated if user is editing the template, to track if template name already exists in system.
var selectedTemplate = "";
var selectedDocumentType = "";

var preSelected = {
    //x: 0,
    //y: 0,
    //x2: 0,
    //y2: 0,
    //h: 0,
    //w: 0,
    //p: 0,
    //fieldName: ""
};

var pageDetails = {
    totalPages: 0,
    currentPage: 1,
}

var canvasDetails = {
    height: 0,
    width: 0
}

$(document).ready(function () {
    LoadDocumentType();

    //Load template if it's requested to edit
    
    $("#btnAddField").click(function () {
        ordinal = $(".field-container").length + 1

        AddField();
    })
    
    $("#fileTemplate").change(function () {
        fileData = document.getElementById('fileTemplate').files[0]

        $("#file-preview").html("<canvas id='canvas-file'></canvas>")
        $("#fields-container").html("");

        LoadImgFromSrc();

        let fileType = $(this).val().split(".");
        fileType = fileType[fileType.length - 1];
        
        if (fileType.toUpperCase() == "PDF") {
            currentFileType = "PDF"
        } else {
            currentFileType = "IMG"
        }
    });

    $("#btnSaveTemplate").click(function () {
        Swal.fire({
            title: 'Are you sure?',
            text: "Do you want to save the template now?",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, save it!'
        }).then((result) => {
            if (result.value) {
                SaveTemplate();
            }
        })
    })

    $("#btnNext").click(function () {
        if (pageDetails.currentPage < pageDetails.totalPages) {
            pageDetails.currentPage += 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })

    $("#btnPrev").click(function () {
        if (pageDetails.currentPage > 1) {
            pageDetails.currentPage -= 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })

    if (templateIdparam > 0) {
        LoadTemplate(templateIdparam);
        $("#cbxDocumentType").attr("disabled", true);
        $("#txtTemplateName").attr("disabled", true);
        $("#mode").html("Edit");
    }
})

function AddField(coord) {
    let text;

    if (coord) {
        text = "Edit"
    } else {
        text = "Set"
    }

    coord = coord || {}
    
    coord.fieldName = coord.fieldName || "";
    ordinal = $(".field-container").length + 1;

    let attributes = "x='" + coord.x + "' y='" + coord.y + "' x2='" + (parseInt(coord.x) + parseInt(coord.w)) + "' " +
        "y2='" + (parseInt(coord.y) + parseInt(coord.h)) + "' h='" + coord.h + "' w='" + coord.w + "' " +
        "p='" + coord.p + "'";

    $("#fields-container").append(
        "<div class='field-container'>" +
        "<div class='field-name d-flex align-items-center'>" +
        "<div>" +
        "<label class='mr-2 mb-0'>Field Name : </label>" +
        "</div>" +
        "<div>" +
        "<input value='" + coord.fieldName + "' type='text' class='form-control form-control-sm input-field-input mr-2 txtFieldName' />" +
        "</div>" +
        "<div>" +
        "<button " + attributes + " ordinal='" + ordinal + "' onclick='SetCoordinate(this)' class='btn btn-success btn-sm btnSaveField'>"+ text +"</button>" +
        "<button ordinal='" + ordinal + "' onclick='RemoveCoordinate(this)' class='btn btn-danger btn-sm btnRemoveCoordinate'>Remove</button>" +
        "</div>" +
        "</div>" +
        "</div>"
    )
}

function PreviewSelected(options) {
    /*
        options = {
            canvas, --element
            preview : {
                top:0,
                left:0,
                width:0,
                height0,
            },
            x,
            y,
            zoom: 1.5,
            distance: 1
        }      
    */
    
    let preview = options.preview;

    //setting the size

    let height = parseFloat(preview.height) * parseFloat(options.zoom);
    let width = parseFloat(preview.width) * parseFloat(options.zoom);
    let bgHeight = parseFloat(canvasDetails.height) * parseFloat(options.zoom);
    let bgWidth = parseFloat(canvasDetails.width) * parseFloat(options.zoom);
    let x = parseFloat(options.x) * parseFloat(options.zoom);
    let y = parseFloat(options.y) * parseFloat(options.zoom);
    let top = parseFloat(options.preview.top) + parseFloat(options.distance);
    let left = parseFloat(options.preview.left) + parseFloat(options.distance);
    
    preview.element.css("display", "block").css("top", top + "px").css("left", left)
    preview.element.css("height", height).css("width", width);

    preview.element.css("background-position-x", "-" + x + "px").css("background-position-y", "-" + y + "px");
    preview.element.css("background-size", bgWidth + "px" + " " + bgHeight + "px")


    let previewDocPosition = document.getElementById("img-preview").getBoundingClientRect();
    let parentDocPosition = document.getElementById("file-container").getBoundingClientRect();


    //if the preview offsets, these codes will handle it

    let offsetX = (previewDocPosition.left + previewDocPosition.width) - (parentDocPosition.left + parentDocPosition.width) + 30
    let offsetY = (previewDocPosition.top + previewDocPosition.height) - (parentDocPosition.top + parentDocPosition.height) + 30

    //adjust to left if overflown horizontally
    if (offsetX > 0) {
        preview.element.css("left", (left - offsetX));
    }

    //adjust to the top if overflown vertically but not horizontally
    if (offsetY > 0 & offsetX <= 0) {
        preview.element.css("top", (top - offsetY));
    }

    //move to the top of cropping selection if overflown both horizontally and vertically
    if (offsetY > 0 & offsetX > 0) {
        preview.element.css("top", options.y - previewDocPosition.height - 20);
    }
}

function LoadDocumentType() {
    $.ajax({
        url: "/DocumentType/GetDocumentTypeList",
        type: "GET",
        async: false,
        success: function (data) {
            $.each(data, function (i, item) {
                $("#cbxDocumentType").append(
                    "<option value='" + item.Id + "'>" + item.DocumentTypeName + "</option>"
                )
            })
        }
    })
}

function LoadImgFromSrc(src) {
    var input = document.getElementById("fileTemplate");

    $("#page-config-area").removeClass("d-flex").addClass("d-none");

    if (input.files && input.files[0]) {
        currentFileExtension = input.files[0].name.split('.').pop().toLowerCase()

        var reader = new FileReader();

        reader.onload = function (e) {
            SetImgOnCanvas(e.target.result);
        }

        reader.readAsDataURL(input.files[0]);

    } else if (templateIdparam > 0) {
        SetImgOnCanvas(src)
    }
}

function SetImgOnCanvas(src, onRender) {
    onRender = onRender || function () { };

    if (currentFileExtension == "pdf") {
        currentFile = pdfjsLib.getDocument(src);
        
        pageDetails.currentPage = 1;
        LoadPDFAsImage(onRender);
    } else {
        var canvasContext = document.getElementById('canvas-file').getContext('2d');
        currentFile = new Image();
        currentFile.src = src;

        currentFile.onload = function () {
            canvasContext.canvas.height = currentFile.height;
            canvasContext.canvas.width = currentFile.width;
            canvasDetails.height = currentFile.height;
            canvasDetails.width = currentFile.width;

            canvasContext.drawImage(currentFile, 0, 0);

            SetPreview();
            onRender();
        }
    }
}

function SetPreview() {
    let canvas = document.getElementById("canvas-file");
    var src = canvas.toDataURL();
    $("#img-preview").css("background", "url(" + src + ")")
}

function LoadPDFAsImage(onRender) {
    $("#page-config-area").removeClass("d-none").addClass("d-flex");
    onRender = onRender || function () { };
    
    currentFile.promise.then(function (pdf) {
        pageDetails.totalPages = pdf.numPages;
        pdf.getPage(pageDetails.currentPage).then(function (page) {
            var scale = 1;
            var viewport = page.getViewport({ scale: scale, });
            //
            // Prepare canvas using PDF page dimensions
            //
            var canvas = document.getElementById('canvas-file');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            canvasDetails.height = viewport.height;
            canvasDetails.width = viewport.width;

            //
            // Render PDF page into canvas context
            //
            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };

            var renderTask = page.render(renderContext);

            renderTask.promise.then(function () {
                onRender();
                SetPreview();
            })
            
            $("#lblPageDetails").html(pageDetails.currentPage + " of " + pageDetails.totalPages);
        });
    });
}

function DisplayPageDetails() {
    $("#lblPageDetails").html(pageDetails.currentPage + " of " + pageDetails.totalPages);
}

function ResetCanvas() {
    $("#file-preview").html("<canvas id='canvas-file'></canvas>");
    var canvasContext = document.getElementById('canvas-file').getContext('2d');
    canvasContext.canvas.height = currentFile.height;
    canvasContext.canvas.width = currentFile.width;
    canvasContext.drawImage(currentFile, 0, 0);
}

function RemoveCoordinate(that) {
    if ($(that).html() == "Remove") {
        $(that).closest(".field-container").remove();
    } else if ($(that).html() == "Cancel") {
        $(that).html("Remove");
        let parentContainer = $(that).closest(".field-container");
        OverlayOff();
        jcrop_api.release();
        jcrop_api.disable();
        $("#img-preview").css("display", "none")

        let btnSave = $(parentContainer).find(".btnSaveField");
        $(btnSave).removeClass("btn-primary").addClass("btn-success");
        $(parentContainer).removeClass("active");

        if (preSelected.w) {
            $(btnSave).html("Edit")

            $(btnSave).attr("x", preSelected.x);
            $(btnSave).attr("y", preSelected.y);
            $(btnSave).attr("w", preSelected.w);
            $(btnSave).attr("h", preSelected.h);
            $(btnSave).attr("x2", preSelected.x2);
            $(btnSave).attr("y2", preSelected.y2);
            $(btnSave).attr("p", preSelected.p);
        } else {
            $(btnSave).html("Set")
        }
    }

    if (currentFileType == "PDF") {
        LoadPDFAsImage(function () {
            return RedrawRectangle()
        })
    } else {
        ResetCanvas();
        RedrawRectangle();
    }
}

function SetCoordinate(that) {
    if ($("#fileTemplate").val() == "" && templateIdparam == 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please choose a file first!'
        })

        return false;
    }

    let parentContainer = $(that).closest(".field-container");
    $(parentContainer).addClass("active");
    $(parentContainer).find(".btnRemoveCoordinate").html("Cancel");
    $(".field-container").not(parentContainer).removeClass("active");

    if ($(that).html() == "Set") {
        preSelected = {};
        OverlayOn();
        $("#canvas-file").Jcrop({
            bgColor: 'black',
            bgOpacity: .4,
            onChange: function (coords) {
                let previewOpt = {
                    canvas: document.getElementById("canvas-file"),
                    preview: {
                        element: $("#img-preview"),
                        top: coords.y2,
                        left: coords.x2,
                        width: coords.w,
                        height: coords.h
                    },
                    x: coords.x,
                    y: coords.y,
                    zoom: 1.75,
                    distance: 10 //in px
                }

                PreviewSelected(previewOpt);
                
                $(that).attr("x", coords.x)
                $(that).attr("x2", coords.x2)
                $(that).attr("y", coords.y)
                $(that).attr("y2", coords.y2)
                $(that).attr("h", coords.h)
                $(that).attr("w", coords.w)
                $(that).attr("p", pageDetails.currentPage)

                $(that).html("Save").addClass("btn-primary").removeClass("btn-success");
            }
        }, function () {
            jcrop_api = this;
        });
        jcrop_api.enable();
    } else if ($(that).html() == "Save") {
        let fieldName = $(parentContainer).find(".txtFieldName").val();

        if (fieldName == "") {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please set the field name first!'
            })

            return false;
        }

        var canvasContext = document.getElementById('canvas-file').getContext('2d');
        let h = $(that).attr("h");
        let w = $(that).attr("w");
        let x = $(that).attr("x");
        let y = $(that).attr("y");
        let p = $(that).attr("p");

        jcrop_api.release();
        jcrop_api.disable();
        $("#img-preview").css("display", "none")
        DrawRectangleOnCanvas(canvasContext, h, w, x, y, p, fieldName); //p -> page

        $(that).html("Edit").removeClass("btn-primary").addClass("btn-success");
        $(parentContainer).removeClass("active");
        $(".btnRemoveCoordinate").attr("disabled", false)

        OverlayOff();
    } else if ($(that).html() == "Edit") {
        let x = $(that).attr("x")
        let y = $(that).attr("y")
        let x2 = $(that).attr("x2")
        let y2 = $(that).attr("y2")
        let w = $(that).attr("w")
        let h = $(that).attr("h")
        let p = $(that).attr("p");
        let fieldName = $(parentContainer).find(".txtFieldName").val();

        preSelected.h = h;
        preSelected.w = w;
        preSelected.x = x;
        preSelected.y = y;
        preSelected.x2 = x2;
        preSelected.y2 = y2;
        preSelected.p = p;
        preSelected.fieldName = fieldName;
        
        //to exempt from redrawing of rectangle
        let currentOrdinal = $(that).attr("ordinal")

        //Must reset the canvas to edit the field, which is according by current file type loaded.

        if (currentFileType == "PDF") {
            LoadPDFAsImage(function () {
                return RedrawRectangle({ exemption: [currentOrdinal] })
            })
        } else {
            ResetCanvas();
            RedrawRectangle({ exemption: [currentOrdinal] });
        }

        var canvasContext = document.getElementById('canvas-file').getContext('2d');

        OverlayOn();

        $("#canvas-file").Jcrop({
            bgColor: 'black',
            bgOpacity: .4,
            setSelect: [x, y, x2, y2],
            onChange: function (coords) {
                let previewOpt = {
                    canvas: document.getElementById("canvas-file"),
                    preview: {
                        element: $("#img-preview"),
                        top: coords.y2,
                        left: coords.x2,
                        width: coords.w,
                        height: coords.h
                    },
                    x: coords.x,
                    y: coords.y,
                    zoom: 1.75,
                    distance: 10 //in px
                }

                PreviewSelected(previewOpt);

                $(that).attr("x", coords.x)
                $(that).attr("x2", coords.x2)
                $(that).attr("y", coords.y)
                $(that).attr("y2", coords.y2)
                $(that).attr("h", coords.h)
                $(that).attr("w", coords.w)
                $(that).attr("p", pageDetails.currentPage)

                $(that).html("Save").addClass("btn-primary").removeClass("btn-success");
            }
        }, function () {
            jcrop_api = this;
        });
        jcrop_api.enable();

        let previewOpt = {
            canvas: document.getElementById("canvas-file"),
            preview: {
                element: $("#img-preview"),
                top: y2,
                left: x2,
                width: w,
                height: h
            },
            x: x,
            y: y,
            zoom: 1.75,
            distance: 10 //in px
        }

        setTimeout(function () {
            PreviewSelected(previewOpt);
        }, 30);
    }
}

function LoadTemplate(templateId) {
    $.ajax({
        url: "/Template/GetTemplateInfo?templateId=" + templateId,
        success: function (data) {
            selectedTemplate = data.Template.TemplateName;
            selectedDocumentType = data.Template.DocumentTypeId;

            $("#cbxDocumentType").val(data.Template.DocumentTypeId);
            $("#txtTemplateName").val(data.Template.TemplateName);
            $.each(data.TemplateDetails, function (i, item) {
                var coords = {
                    x: item.XCoordinate,
                    y: item.YCoordinate,
                    w: item.Width,
                    h: item.Height,
                    fieldName: item.FieldName,
                    p: item.Page
                }

                AddField(coords);
            })
            currentFileExtension = data.Template.FileExtension

            if (currentFileExtension.toUpperCase() == "PDF") {
                currentFileType = "PDF"
            } else {
                currentFileType = "IMG";
            }

            SetImgOnCanvas("/Template Files/" + data.Template.Id + "." + currentFileExtension, RedrawRectangle);
        }
    })
}

function DrawRectangleOnCanvas(canvasContext, height, width, x, y, p, field, isText) { //p -> page
    isText = isText || false;

    if (pageDetails.currentPage == p) { //Draw rectangle only on current page
        height = parseInt(height);
        width = parseInt(width);
        x = parseInt(x);
        y = parseInt(y);

        canvasContext.beginPath();
        canvasContext.rect(x, y, width, height);
        canvasContext.strokeStyle = "red";
        canvasContext.stroke();
        canvasContext.font = "9pt sans-serif";

        if (isText) {
            canvasContext.fillStyle = "red";
            canvasContext.fillText(field, (x + width) + 3, y + ((height / 2) + 3))
        }
    }
}

function RedrawRectangle(options) {
    options = options || {};
    options.exemption = options.exemption || [];

    var canvasContext = document.getElementById('canvas-file').getContext('2d');

    $.each(GetSetCoodinates(), function (i, item) {
        if (options.exemption.indexOf(item.ordinal) == - 1 && item.XCoordinate > 0) {
            DrawRectangleOnCanvas(canvasContext, item.Height, item.Width, item.XCoordinate, item.YCoordinate, item.Page, item.FieldName)
        }
    });
}

function GetSetCoodinates() {
    let coorArr = []

    $(".field-container").each(function () {
        let that = $(this).find(".btnSaveField")
        let field = $(this).find(".txtFieldName").val()
        let x = $(that).attr("x");
        let y = $(that).attr("y");
        let x2 = $(that).attr("x2");
        let y2 = $(that).attr("y2");
        let h = $(that).attr("h");
        let w = $(that).attr("w");
        let p = $(that).attr("p");
        let ordinal = $(that).attr("ordinal");

        coorArr.push({
            XCoordinate: x,
            YCoordinate: y,
            Height: h,
            Width: w,
            Page: p,
            FieldName: field,
            ordinal: ordinal
        })
    })

    return coorArr
}

function ClearRectangle(canvasContext, height, width, x, y) {
    canvasContext.beginPath();
    canvasContext.clearRect(x, y, width, height);
}

function ReadImage(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function OverlayOn() {
    document.getElementById("overlay").style.display = "block";
}

function OverlayOff() {
    document.getElementById("overlay").style.display = "none";
}

function SaveTemplate() {
    let fieldArr = [];
    let templateName = $("#txtTemplateName").val();
    let documentTypeId = $("#cbxDocumentType").val();
    let canvasContext = document.getElementById('canvas-file').getContext('2d');
    let pageHeight = canvasContext.canvas.height;
    let pageWidth = canvasContext.canvas.width;
    
    $(".field-container").each(function () {
        let that = $(this).find(".btnSaveField");
        let field = $(this).find(".txtFieldName").val();

        if (field != "" && $(that).attr("x") > 0) {
            fieldArr.push({
                FieldName: field,
                Height: $(that).attr("h"),
                Width: $(that).attr("w"),
                XCoordinate: $(that).attr("x"),
                YCoordinate: $(that).attr("y"),
                Page: $(that).attr("p")
            })
        }
    })

    if (!templateName) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please set a template name first!'
        })

        return false;
    }

    if (!documentTypeId) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please set the document type!'
        })

        return false;
    }

    if (fieldArr.length == 0) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please add at least one field!'
        })

        return false;
    }

    HoldOn.open({
        theme: "sk-circle"
    })

    console.log(templateName + " " + selectedTemplate);
    console.log(selectedDocumentType + " " + documentTypeId);
    
    console.log((templateName != selectedTemplate || selectedDocumentType != documentTypeId))

    $.ajax({
        url: "/Template/AddTemplate",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: JSON.stringify({
            t: {
                TemplateName: templateName,
                DocumentTypeId: documentTypeId,
                HasTemplateNameChanged: (templateName != selectedTemplate || selectedDocumentType != documentTypeId),
                PageHeight: pageHeight,
                PageWidth: pageWidth,
                FileExtension: currentFileExtension,
                Id: templateIdparam
            },
            tdArr: fieldArr
        }),
        success: function (data) {
            if (data.Status == "FAILED") {
                HoldOn.close()

                Swal.fire({
                    icon: "error",
                    title: "Saving Failed",
                    text: "There is an error processing your request."
                })

                return false;
            }
            else if (data.Status == "NAME ALREADY EXISTS")
            {
                HoldOn.close()

                Swal.fire({
                    icon: "error",
                    title: "Saving Failed",
                    text: "Template name already exists in the system."
                })

                return false;
            }


            var formData = new FormData();
            formData.append("FileUpload", fileData);
            formData.append("templateId", data.Id);

            if (data.Id > 0) {
                //file data would not be blank if the user is just going to modify the fields of pre-saved template
                if (fileData != "") {
                    $.ajax({
                        url: '/Template/SaveTemplateFile',
                        type: "POST",
                        contentType: false,
                        processData: false,
                        data: formData,
                        success: function (data) {
                            HoldOn.close()
                            if (data == "SUCCESS") {
                                Swal.fire({
                                    icon: 'success',
                                    title: 'Success',
                                    text: 'The template is saved successfully!'
                                }).then(function () {
                                    window.location.href = "/Template"
                                });
                            } else {
                                Swal.fire({
                                    icon: 'error',
                                    title: 'Oops...',
                                    text: 'There is an error processing your request.'
                                })
                            }
                        }
                    });
                } else {
                    HoldOn.close()
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: 'The template is saved successfully!'
                    }).then(function () {
                        window.location.href = "/Template"
                    });
                }
            } else {
                Swal.fire({
                    icon: 'error',
                    title: 'Oops...',
                    text: 'There is an error processing your request.'
                })
            }
        }
    })
}



