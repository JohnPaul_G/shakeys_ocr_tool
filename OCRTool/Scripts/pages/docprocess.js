﻿var documentTypeOpts = "";
var processId = 0;

$.ajax({
    url: "/DocumentType/GetDocumentTypeList",
    async: false,
    success: function (data) {
        $.each(data, function (i, item) {
            documentTypeOpts += "<option value='" + item.Id + "'>" + item.DocumentTypeName + "</option>"
        })
    }
})

$(document).ready(function () {
    $("#sortable_items").sortable({
        update: function () {
            UpdateOrder();
        }
    });

    $("#dtProcess").DataTable({
        ajax: {
            url: "/DocProcess/GetDocProcessList",
            dataType: "json",
            dataSrc: ''
        },
        columns: [
            { title: "Process", data: "DocProcessName" },
            {
                title: "Action",
                data: null,
                render: function (data) {
                    let details = "<button onclick='EditProcess(" + JSON.stringify(data) + ")' class='btn btn-sm btn-success'>Edit</button>"
                    return details;
                }
            }
        ]
    })

    $("#btnAdd").click(function () {
        processId = 0;

        $("#sortable_items").html(GenerateProcessItemHtml());

        $("#modalDocProcess .modal-title").html("Add a new Document Process");

        $("#txtProcessName").val("");
        
        SetSelect2();

        $("#modalDocProcess").modal();
    })

    $("#btnAddField").click(function () {
        $("#sortable_items").append(GenerateProcessItemHtml());

        SetSelect2();

        UpdateOrder();
    })

    $("#btnSaveProcess").click(function () {
        SaveProcess();
    })
})

function RemoveItem(that) {
    $(that).closest(".draggable").remove();
    UpdateOrder();
}

function UpdateOrder() {
    let ordinal = 1;

    $(".item-ordinal").each(function () {
        $(this).html(ordinal + ".")
        ordinal++;
    });
}

function SetSelect2() {
    $('.cboDocumentType').select2({
        dropdownParent: $("#modalDocProcess"),
        width: '100%'
    }).on("select2:unselecting", function (e) {
        console.log("test");
    }
    );
}

//this function is to disallow duplicate documentType entry in a process setup
function ValidateDocumentTypeOption(that) {
    let preSelected = $(that).val();
    let isValidated = true;

    $(".cboDocumentType").not(that).each(function () {
        if ($(this).val() == preSelected) {
            isValidated = false;
        }
    })

    if (isValidated == false) {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Duplicate entry for documentTypes are not allowed!'
        });

        $(that).val("");
        $(that).closest("div").find(".select2-selection__rendered").html("Please choose a document type");
    }
}

function AllowOnlyNumbers() {
    $(this).keydown(function (e) {
        var key = e.charCode || e.keyCode || 0;
        // allow backspace, tab, delete, enter, arrows, numbers and keypad numbers ONLY
        return (
            key == 8 ||
            key == 9 ||
            key == 13 ||
            key == 46 ||
            key == 110 ||
            (key >= 35 && key <= 40) ||
            (key >= 48 && key <= 57) ||
            (key >= 96 && key <= 105));
    });
}

function GenerateProcessItemHtml(documentType, minQty) {
    documentType = documentType || {};
    minQty = minQty || 1;

    var itemHtml = '<div class="draggable position-relative">' +
        '<i onclick="RemoveItem(this)" class="fa fa-window-close position-absolute close-item"></i>' +
        '<div class="d-flex">' +
        '<div class="col-9 d-flex align-items-center">' +
        '<div>' +
        '<label class="mb-0 lblDocumentType"> <span class="item-ordinal">1. </span> DocumentType : </label>' +
        '</div>' +
        '<div class="w-100">' +
        '<select onchange="ValidateDocumentTypeOption(this)" class="form-control form-control-sm cboDocumentType">' +
        (documentType.Id > 0 ? '<option selected value="' + documentType.Id + '">' + documentType.DocumentTypeName +
            '</option>' : '<option value="" selected disabled>Please choose a documentType</option>') +
        documentTypeOpts +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="col-3 d-flex align-items-center">' +
        '<div>' +
        '<label class="mb-0 lblMinQty">Min Qty :</label>' +
        '</div>' +
        '<div>' +
        '<input class="form-control form-control-sm txtMinQty" value="' + minQty + '" onkeydown="AllowOnlyNumbers()" type="number" />' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>';

    return itemHtml
}

function EditProcess(data) {
    processId = data.Id;

    $("#modalDocProcess .modal-title").html(data.DocProcessName);
    $("#txtProcessName").val(data.DocProcessName);
    $("#sortable_items").html("");

    $.ajax({
        url: "/DocProcess/GetDocProcessItemsList?docProcessId=" + processId,
        success: function (result) {
            $.each(result, function (i, item) {
                $("#sortable_items").append(
                    GenerateProcessItemHtml(
                        {
                            Id: item.DocumentType.Id,
                            DocumentTypeName: item.DocumentType.DocumentTypeName
                        },
                        item.MinQty
                    )
                )
            })

            SetSelect2();

            UpdateOrder();

            $("#modalDocProcess").modal();
        }
    })
}

function SaveProcess() {
    let processName = $("#txtProcessName").val();
    let order = 1;
    let processItems = [];
    let isBlankThere = false;

    $(".draggable").each(function () {
        let documentType = $(this).find(".cboDocumentType").val();
        let minQty = $(this).find(".txtMinQty").val();

        if (!documentType || !(minQty > 0)) {
            isBlankThere = true;
        }

        processItems.push({
            DocumentTypeId: documentType,
            MinQty: minQty,
            Order: order++
        })
    })

    if (isBlankThere || processItems.length == 0 || processName == "") {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please complete all of the inputs with valid entry before saving!'
        });

        return false;
    }

    Swal.fire({
        title: 'Are you sure?',
        text: "Do you want to save the process now?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, save it!'
    }).then((result) => {
        if (result.value) {
            HoldOn.open({
                theme: "sk-circle"
            })

            $.ajax({
                url: "/DocProcess/AddUpdateDocProcess",
                type: "POST",
                dataType: "json",
                contentType: "application/json",
                data: JSON.stringify({
                    dp: {
                        Id: processId,
                        DocProcessName: processName
                    },
                    dpiArr: processItems
                }),
                success: function (data) {
                    if (data == "SUCCESS") {
                        Swal.fire({
                            icon: 'success',
                            title: 'Success',
                            text: 'The process is saved successfully!'
                        }).then(function () {
                            window.location.reload();
                        });
                    } else {
                        Swal.fire({
                            icon: 'error',
                            title: 'Oops...',
                            text: 'There is an error processing your request.'
                        })
                    }
                },
                complete: function () {
                    HoldOn.close();
                }
            })
        }
    })
}
