﻿$(document).ready(function () {
    LoadTable();

    $("#dtrSearch").daterangepicker();
})

function LoadTable(dateFrom, dateTo) {
    HoldOn.open();

    $("#dtSystemLogs").DataTable().destroy();
    $("#dtSystemLogs").DataTable({
        destroy: true,
        ajax: {
            url: "/System/GetSystemLogsList?dateFrom="+ dateFrom +"&dateTo=" + dateTo,
            dataType: "json",
            dataSrc: ''
        },
        columns: [
            {
                title: "Date/Time",
                data: null,
                render: function (data) {
                    let details = FormatDate(data.DTLog, "MM/dd/yyyy hh:mm TT");
                    return details;
                }
            },
            {
                title: "User",
                data: null,
                render: function (data) {
                    let details = data.User || "Unknown";
                    return details;
                }
            },
            { title: "Description", data: "Description" },
            {
                title: "Status",
                data: null,
                render: function (data) {
                    let details;

                    if (data.Status == "FAILED") {
                        details = `<span class="kt-badge kt-badge--danger kt-badge--inline">Failed</span>`;
                    } else {
                        details = `<span class="kt-badge kt-badge--success kt-badge--inline">Success</span>`;
                    }
                    return details;
                }
            }
        ],
        columnDefs: [
            {
                targets: [0, 1, 2],
                class: "text-left"
            },
            {
                targets: [3],
                class: "text-center"
            }
        ],
        initComplete: function () {
            HoldOn.close();
        }
    })
}

function SearchLogs() {
    var daterange = ($("#dtrSearch").val()).split(" - ");
    var dateFrom = daterange[0];
    var dateTo = daterange[1];

    LoadTable(dateFrom, dateTo);
}