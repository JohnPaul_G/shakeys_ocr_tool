﻿var templateList;
var documentTypeList;
var directoryList;
var storeList;
var user;
var userDetails;

//used for signalR
var connectionId = '';

var currentFileExtension;

var currentFilename = "";

var currentTemplate;

var currentFile;

//used to track the details of scanning - id, scanned file, ocr result, failures
var jobStatusDetailsList = [];

//for viewing files
var pageDetails = {
    totalPages: 0,
    currentPage: 1,
}

var canvasDetails = {
    height: 0,
    width: 0
}

$.ajax({
    async: false,
    url: "/OCRWizard/DeleteUserActiveJobs"
})


$.ajax({
    async: false,
    url: "/Template/GetTemplateList",
    success: function (data) {
        templateList = data;
    }
})

$.ajax({
    async: false,
    url: "/DocumentType/GetDocumentTypeList",
    success: function (data) {
        documentTypeList = data;
    }
})

$.ajax({
    async: false,
    url: "https://dms.shakeys.solutions/api/integration/gtdmstrfldrs/5437264954385422313",
    success: function (data) {
        directoryList = data;
    }
})

$.ajax({
    url: "https://dms.shakeys.solutions/api/integration/gtstrdata/5437264954385422313",
    success: function (data) {
        storeList = data;
    }
})

$.ajax({
    url: "/Account/GetCurrentUserDetails",
    success: function (data) {
        userDetails = data;
    }
})

$.connection.hub.start()
    .done(function (data) {
        connectionId = data.id;
    })
    .fail(function (e) { console.log(e) })

$(document).ready(function () {
    user = $("#username").attr("username");

    var options = {
        startStep: 3,
        clickableSteps: true
    };

    $(".cboDocumentType").append(GenerateOptions(documentTypeList, "Id", "DocumentTypeName"))
    $(".cboTemplate").append(GenerateOptions(templateList, "Id", "TemplateName"));

    SetSelect2();

    var wizard = new KTWizard('kt_wizard_v1', options);

    $("#btnNextStep, #btnPrevStep").click(function () {
        $([document.documentElement, document.body]).animate({
            scrollTop: $("#kt_content").offset().top - 100
        }, 500);
    });

    $("#btnNext").click(function () {
        if (pageDetails.currentPage < pageDetails.totalPages) {
            pageDetails.currentPage += 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })

    $("#btnPrev").click(function () {
        if (pageDetails.currentPage > 1) {
            pageDetails.currentPage -= 1;
            LoadPDFAsImage(RedrawRectangle);
        }
    })

    $("#btnSaveContent").click(function () {
        var content = [];

        $("#result-area input").each(function () {
            content.push({
                Fieldname: $(this).attr("fieldname"),
                Text: $(this).val()
            })
        })

        EditFileContent(currentFilename, content);

        RefreshOCRTable();

        $("#modalFile").modal('toggle');
    })

    SetSignalR();

    $("select").select2({ width: '100%' });

    $("#btnSubmit").click(function () {
        Swal.fire({
            title: 'Are you sure?',
            text: "Are you sure that you want to publish now?",
            icon: 'warning',
            showCancelButton: true,
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, publish it!'
        }).then((result) => {
            if (result.value) {
                Publish();
            }
        })
    })
})


//this function is to add another set of files
function AddSet() {
    ordinal = parseInt($(".setup-details").length) + 1;

    $("#step-1_content").append('<div ordinal=' + ordinal + ' class="setup-details mb-md-5 position-relative">' +
        '<div class="row">' +
        '<div class="col-lg-6 col-md-12 mb-lg-0 mb-md-3">' +
        '<div class="form-group mb-0">' +
        '<div class="d-flex justify-content-between align-items-center mb-1">' +
        '<label class="mb-0">Document Type:</label>' +
        '<div class="d-flex align-items-center">' +
        '<div onclick="DeleteSet(this)" class="btnDeleteSet">' +
        '<i class="fa flaticon2-trash"></i>' +
        '</div>' +
        '<div class="dropdown dropdown-inline">' +
        '<button type="button" class="btn btn-clean btn-icon btn-sm btn-icon-md btn-directory" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">' +
        '<i class="fa fa-folder"></i>' +
        '</button>' +
        '<div class="dropdown-menu dropdown-menu-right">' +
        '<div class="ml-3">' +
        '<div class="directory-label">' +
        '<span>Files Destination</span> :' +
        '</div>' +
        '<div class="mt-3 mr-3">' +
        '<div class="directory-details-container">' +
        '<div class="directory-details">' +
        '<div>- Please select the document type - </div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<select onchange="UpdateCboTemplate(this)" class="form-control cboDocumentType">' +
        GenerateOptions(documentTypeList, "Id", "DocumentTypeName") +
        '</select>' +
        '</div>' +
        '</div>' +
        '<div class="col-lg-6 col-md-12">' +
        '<div class="form-group mb-0">' +
        '<div class="d-flex justify-content-between align-items-center mb-1" style="height:32px;">' +
        '<label class="mb-0">Template:</label>' +
        '</div>' +
        '<select onchange="UpdateCboDocumentType(this)" class="form-control cboTemplate">' +
        GenerateOptions(templateList, "Id", "TemplateName") +
        '</select>' +
        '</div>' +
        '</div>' +
        '</div>' +
        '<div class="row justify-content-end align-items-center mt-3">' +
        '<div class="scan-status d-none">' +
        '<i class="fas"></i> ' +
        '<span class="main-status"> Scanning : </span>' +
        '<span class="scan-details">0 scanned file</span>' +
        '</div>' +
        '<button type="button" onclick="StartScanning(this)" class="btn btn-sm btn-success mr-3 btnStartScanning">' +
        '<i class="flaticon2-arrow"> </i>Scan' +
        '</button>' +
        '</div>' +
        '</div>')

    SetSelect2();

    LoadAccessSetting(ordinal);
}

function DeleteSet(that) {
    let parent = $(that).closest(".setup-details");
    let jobId = $(parent).find(".btnStartScanning").attr("startJobId");

    if (jobId) {
        if (IsThrProgressingJob(jobId)) {
            Swal.fire({
                icon: "error",
                title: "Oops",
                text: "Please stop the scanning progress first!"
            })
        } else {
            Swal.fire({
                title: 'Are you sure?',
                text: "Are you sure that you want to remove the selected batch? Scanned files on this batch would be deleted!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    DeleteBatch(jobId);

                    $(parent).remove();
                    let ordinal = $(parent).attr("ordinal");
                    $(".access-details[ordinal='" + ordinal + "']").remove();

                    RefreshOCRTable();
                }
            })
        }
    } else {
        $(parent).remove();
        let ordinal = $(parent).attr("ordinal");
        $(".access-details[ordinal='" + ordinal + "']").remove();
    }
}

function SetSignalR() {
    $.connection.oCRResultHub.client.getScanStatus = function (data) {
        //jobStatusDetailsList = {
        //    template: templateId,
        //    jobId: data,
        //    scannedFile: 0,
        //    OCRResultCount: 0,
        //    OCRResult: []
        //}

        let parent = $(".btnStartScanning[jobId='" + data.Id + "']").closest(".setup-details");
        let jobId = $(".btnStartScanning[jobId='" + data.Id + "']").attr("startJobId");

        if (data.Type == "SCANNED FILE") {
            let result = UpdateJob(jobId, "scannedFile", 1);

            let msg = result.scannedFile + " scanned files, " + result.OCRResultCount +
                " OCR results.";

            $(parent).find(".scan-status").find(".main-status").html("Reading OCR : ");
            $(parent).find(".scan-status").find(".scan-details").html(msg);
        }
        else if (data.Type == "OCR RESULT") {
            let ocrResult = {
                Filename: data.Filename,
                Content: data.OCRFileContentList
            }


            UpdateJob(jobId, "OCRResult", ocrResult);
            UpdateJob(jobId, "TemplateDetails", data.TemplateDetails)

            let result = UpdateJob(jobId, "OCRResultCount", 1);

            let msg = result.scannedFile + " scanned files, " + result.OCRResultCount +
                " OCR results."

            let title = "";

            if (result.scannedFile == result.OCRResultCount) {
                if (result.Status == "STOPPED") {
                    let icon = $(parent).find(".scan-status").find(".fas");
                    $(icon).removeClass("fa-spinner").removeClass("fa-spin").addClass("fa-info-circle");

                    title = "Stopped : "
                } else {
                    title = "Scanning : "
                }
            } else {
                if (result.Status == "STOPPED") {
                    title = "Stopped, Reading OCR : "
                } else {
                    title = "Reading OCR : "
                }
            }

            $(parent).find(".scan-status").find(".main-status").html(title);
            $(parent).find(".scan-status").find(".scan-details").html(msg);

            RefreshOCRTable();

        } else if (data.Type == "STOPPED SCANNING") {
            let result = UpdateJob(jobId, "Status", "STOPPED");

            $(".btnStartScanning[startJobId='" + jobId + "']").html("<i style='font-size:10px;' class='flaticon2-arrow'></i> Start").removeClass("btn-danger").addClass("btn-success");
            $(".btnStartScanning[startJobId='" + jobId + "']").attr("onclick", "StartScanning(this)")

            if (result.scannedFile == result.OCRResultCount) {
                let icon = $(parent).find(".scan-status").find(".fas");

                $(icon).removeClass("fa-spinner").removeClass("fa-spin").addClass("fa-info-circle");

                $(parent).find(".scan-status").find(".main-status").html("Stopped : ");
            } else {
                $(parent).find(".scan-status").find(".main-status").html("Stopped, Reading OCR : ");
            };
        } else if (data.Type == "ERROR") {
            console.log(data);
        }
    }
}

function DeleteHangfireJob(jobId) {
    $.ajax({
        url: "/OCRWizard/DeleteBackgroundJob?jobId=" + jobId
    })
}

function StopScanning(that) {
    let parent = $(that).closest(".setup-details");
    let jobId = $(that).attr("jobId");
    $(parent).find(".scan-status").find(".main-status").html("Stopping : ");
    $(that).html("<i style='font-size:10px;' class='flaticon2-arrow'></i> Start").removeClass("btn-danger").addClass("btn-success");
    $(that).attr("onclick", "StartScanning(this)")

    DeleteHangfireJob(jobId);
}

function StartScanning(that) {
    if (IsThrProgressingJob()) {
        Swal.fire({
            icon: "error",
            title: "Oops",
            text: "Please stop the other scanning progress before starting a new one."
        })

        return false;
    }


    let parent = $(that).closest(".setup-details");
    let templateId = $(parent).find(".cboTemplate").val();

    if (templateId > 0) {
        $(that).attr("onclick", "StopScanning(this)")
        $(parent).find(".scan-status").find(".main-status").html("Scanning : ")
        $(that).html("<i style='font-size:10px;' class='flaticon2-cancel-music'></i> Stop").removeClass("btn-success").addClass("btn-danger");
        $(parent).find(".scan-status").removeClass("d-none");
        $(parent).find(".scan-status").find(".fas").removeClass("fa-info-circle").addClass("fa-spinner fa-spin");
        $(that).closest("div").removeClass("justify-content-end").addClass("justify-content-between");

        $(parent).find(".cboDocumentType").attr("disabled", "disabled");
        $(parent).find(".cboTemplate").attr("disabled", "disabled");

        $.ajax({
            url: "/OCRWizard/StartScanning?templateId=" + templateId + "&srConnectionId=" + connectionId,
            success: function (data) {
                if (data == "FAILED") {
                    Swal.fire({
                        icon: 'error',
                        title: 'Oops...',
                        text: 'There is an error processing your request. Try to logout and relog your account.'
                    })
                } else {
                    //only create a new job list if it is a fresh start
                    if ($(that).attr("startJobId")) {
                        UpdateJob($(that).attr("startJobId"), "Status", "IN PROGRESS");
                    } else {
                        $(that).attr("startJobId", data);

                        jobStatusDetailsList.push({
                            template: GetRowFromJsonByValue(templateList, "Id", templateId),
                            ordinal: $(parent).attr("ordinal"),
                            jobId: data,
                            scannedFile: 0,
                            OCRResultCount: 0,
                            TemplateDetails: [],
                            OCRResult: [],
                            Status: "IN PROGRESS"
                        })
                    }

                    $(that).attr("jobId", data);
                }
            }
        })
    } else {
        Swal.fire({
            icon: 'error',
            title: 'Oops...',
            text: 'Please choose a template first!'
        })
    }
}

//this update the template select element based on the selected document type
function UpdateCboTemplate(that) {
    let documentTypeId = $(that).val();

    let parent = $(that).closest(".setup-details")

    let cboTemplate = $(parent).find(".cboTemplate");

    $(cboTemplate).html("");

    if (documentTypeId == "") {
        $(cboTemplate).html(GenerateOptions(templateList, "Id", "TemplateName"));
    } else {
        let filteredTemplateList = FilterJson(templateList, ["DocumentType", "Id"], documentTypeId)

        $(cboTemplate).html(GenerateOptions(filteredTemplateList, "Id", "TemplateName"));
    }

    GetDMSDirectoryByDT(documentTypeId, $(parent).find(".directory-details"));

    SetSelect2();
}

//this function is just to update the directory path shown in ui
function GetDMSDirectoryByDT(documentTypeId, that) {
    if (documentTypeId == "") {
        $(that).html("- Please select the document type -");
        return false;
    }

    let documentType = GetRowFromJsonByValue(documentTypeList, "Id", documentTypeId);
    let directoryId = documentType.DefaultDirectoryId;
    let hasParent = true;
    let directoryPathArr = [];
    let index = 0;

    while (hasParent) {
        let details = {};
        let directory = GetRowFromJsonByValue(directoryList, "Id", directoryId);
        let parentId = directory.ParentId;

        details.id = directoryId;
        details.text = directory.Foldername;

        if (directory.ParentId) {
            details.parent = directory.ParentId
            directoryId = directory.ParentId;
        } else {
            details.parent = "#";
            hasParent = false;
        }

        if (index == 0) {
            details.state = {
                'opened': true,
                'selected': true
            }
        }

        directoryPathArr.push(details);
        index++;
    }

    $(that).html("<div id='file-tree'></div>")

    $(that).jstree("destroy");

    $(that).jstree({
        'core': {
            'data': directoryPathArr
        }
    });
}

function UpdateCboDocumentType(that) {
    let templateId = $(that).val();

    if (templateId == "") {
        return false;
    }

    let parent = $(that).closest(".setup-details");
    let cboDocumentType = $(parent).find(".cboDocumentType");

    //here we need to get the documentTypeId to filter the cboDocumentType
    let templateRowDetails = FilterJson(templateList, ["Id"], templateId);
    let documentTypeId = templateRowDetails[0]["DocumentType"]["Id"];

    $(cboDocumentType).val(documentTypeId);

    GetDMSDirectoryByDT(documentTypeId, $(parent).find(".directory-details"));

    SetSelect2();
}

function GetRowFromJsonByValue(json, field, value) {
    let result = [];

    $.each(json, function (i, item) {
        if (item[field] == value) {
            result = item;
        }
    })

    return result;
}

//filterField should be Array
function FilterJson(json, filterField, filterValue) {
    var result = [];

    $.each(json, function (i, item) {
        if (_.get(item, filterField) == filterValue) {
            result.push(item);
        }
    });

    return result;
}

function GenerateOptions(json, valueField, nameField, placeholder) {
    placeholder = placeholder || "Select"

    let options = "<option disabled selected value=''>" + placeholder + "</option>";

    $.each(json, function (i, item) {
        options += "<option value='" + item[valueField] + "'>" + item[nameField] + "</option>"
    })

    return options;
}

function RefreshOCRTable() {
    $("#set_results").html("");

    $.each(jobStatusDetailsList, function (i, job) {
        if (job.OCRResultCount > 0) {
            let template = GetRowFromJsonByValue(templateList, "Id", job.template.Id);
            let documentType = template.DocumentType;

            let html = '<div class="set-result">' +
                '<div class="set-title text-center">' +
                'Set ' + (i + 1) +
                '</div><div class="table-container">' +
                '<table class="table table-bordered">' +
                '<tr class="set-details">' +
                '<td colspan="100">' +
                '<div class="row justify-content-around">' +
                '<div>' +
                'Template : ' +
                template.TemplateName +
                '</div>' +
                '<div>' +
                'Document Type : ' +
                documentType.DocumentTypeName +
                '</div>' +
                '</div>' +
                '</td>' +
                '</tr>' +
                '<tr class="set-fields">';

            html += "<td>Filename</td>";


            $.each(job.OCRResult[0].Content, function (i2, content) {
                html += "<td>" + content.Fieldname + "</td>";
            })

            html += "<td style='width:80px;'>Action</td>";

            html += "</tr>"

            $.each(job.OCRResult, function (i2, result) {
                html += "<tr class='set-data'>";

                let filename = result.Filename

                html += "<td>" + filename + "</td>"

                $.each(result.Content, function (i3, content) {
                    html += "<td>" + content.Text + "</td>";
                });
                html += "<td ><div><a class='text-primary' onclick=\"Edit('" + job.jobId +
                    "'," + i2 + ")\" href='javascript:void(0)'><i class='flaticon2-writing'></i> Edit</a></div>" +
                    "<div class='mt-2'><a class='text-danger' onclick=\"DeleteFile('" + filename +
                    "')\" href='javascript:void(0)'><i class='flaticon2-trash'></i> Delete</a></div>" + "</td>";

                html += "</tr>";
            })

            html += "</table></div></div>";

            $("#set_results").append(html)
        }
    });
}

function UpdateJob(jobId, field, value) {
    let resultRow = [];

    $.each(jobStatusDetailsList, function (i, item) {
        if (item.jobId == jobId) {
            if (field == "OCRResult") {
                jobStatusDetailsList[i][field].push(value);
            } else if (field == "Status" || field == "TemplateDetails") {
                jobStatusDetailsList[i][field] = value;
            } else {
                jobStatusDetailsList[i][field] += value;
            }

            resultRow = item;
        }
    })

    return resultRow;
}

function DrawRectangleOnCanvas(canvasContext, height, width, x, y, p, field, isText) { //p -> page
    isText = isText || false;

    if (pageDetails.currentPage == p) { //Draw rectangle only on current page
        height = parseInt(height);
        width = parseInt(width);
        x = parseInt(x);
        y = parseInt(y);

        canvasContext.beginPath();
        canvasContext.rect(x, y, width, height);
        canvasContext.strokeStyle = "red";
        canvasContext.stroke();
        canvasContext.font = "9pt sans-serif";

        if (isText) {
            canvasContext.fillStyle = "red";
            canvasContext.fillText(field, (x + width) + 3, y + ((height / 2) + 3))
        }
    }
}

function SetImgOnCanvas(src, onRender) {
    onRender = onRender || function () { };

    if (currentFileExtension == "pdf") {
        currentFile = pdfjsLib.getDocument(src);

        pageDetails.currentPage = 1;
        LoadPDFAsImage(onRender);
    } else {
        var canvasContext = document.getElementById('canvas-file').getContext('2d');
        currentFile = new Image();
        currentFile.src = src;

        currentFile.onload = function () {
            canvasContext.canvas.height = currentFile.height;
            canvasContext.canvas.width = currentFile.width;
            canvasDetails.height = currentFile.height;
            canvasDetails.width = currentFile.width;

            canvasContext.drawImage(currentFile, 0, 0);

            onRender();
        }
    }
}

function IsThrProgressingJob(jobId) {
    jobId = jobId || -1
    let inProgress = false;

    $.each(jobStatusDetailsList, function (i, item) {
        if (item.Status == "IN PROGRESS" && (jobId == - 1 ? true : item.jobId == jobId)) {
            inProgress = true;
        }
    })

    return inProgress;
}

function SetSelect2() {
    $('.cboDocumentType').select2()

    $(".cboTemplate").select2()
}

function Edit(jobId, OCRRIndex) {
    let job = GetRowFromJsonByValue(jobStatusDetailsList, 'jobId', jobId);
    let filename = job.OCRResult[OCRRIndex].Filename;

    currentFilename = filename;

    let contents = job.OCRResult[OCRRIndex].Content;
    currentTemplate = job.TemplateDetails;

    let splitFilename = filename.split(".");
    currentFileExtension = splitFilename[splitFilename.length - 1];

    $("#file-area").html("<canvas id='canvas-file'></canvas>");
    let path = "/Users/" + user + "/OCR Read Files/" + filename

    SetImgOnCanvas(path, RedrawRectangle);

    let temp = "";

    $.each(contents, function (i, item) {
        temp += "<div class='col-lg-12 mb-3'>" +
            "<label class='lbl-field-name'>" + item.Fieldname + " : </label>" +
            "<input fieldname='" + item.Fieldname + "' class='form-control form-control-sm txt-field-data' value='" +
            (item.Text).replace(/'/g, "&#39") + "' />" +
            "</div>"
    });

    $("#filename").html(filename);
    $("#results_container").html(temp);

    $("#modalFile").modal();
}

function AccessClassHandler(that) {
    let parent = $(that).closest(".access-details");
    let selected = $(that).val();

    $(parent).find(".opt-container").not(".document-access-container").not(".access-class-container").remove();

    if (selected == "PRIVATE") {
        let accessTypeElmnt = `<div class="access-type-container opt-container">
                                        <label class="access-title">Type : </label>
                                        <select onchange="AccessTypeHandler(this)" class="form-control form-control-sm access-type">
                                            <option value="" disabled hidden selected>Select Type:</option>
                                            <option value="DepartmentFolder">Department Folder</option>
                                            <option value="Store">Store</option>
                                            <option value="DepartmentFolderStore">Department Folder/Store</option>
                                        </select>
                              </div>`

        $(parent).append(accessTypeElmnt);
        $("select").select2({ width: '100%' });
    } else if (selected == "PUBLIC") {
        let publicAccessElmnt = `<div class="public-access-container opt-container">
                                        <label class="access-title">Public Access :</label>
                                        <div class="ml-4" style="display:flex;">
                                            <div class="mr-4 d-flex align-items-center">
                                                <input value="FRANCHISE"  type="checkbox"> 
                                                <div class="ml-1">Franchise</div>
                                            </div>
                                            <div class="mr-4 d-flex align-items-center">
                                                <input value="CO-OWNED" type="checkbox"> 
                                                <div class="ml-1">Co-Owned</div>
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <input value="CSO" type="checkbox"> 
                                                <div class="ml-1">CSO</div>
                                            </div>
                                        </div>
                                    </div>`

        $(parent).append(publicAccessElmnt);
    } else if (selected == "RESTRICTED") {
        let parentFolders = FilterJson(directoryList, "ParentId", null);

        let restrictedElmnt = `<div class="store-container opt-container d-flex justify-content-center">
                            <div class="col-12">
                            <label class="access-title">Select Department :</label>
                            <select class="form-control select-department form-control-sm">
                            ${GenerateOptions(parentFolders, "Id", "Foldername", "Select Department")}
                            </select>
                            </div>
                            </div>`

        $(parent).append(restrictedElmnt);
        $("select").select2({ width: '100%' });
    }
}

function AccessTypeHandler(that) {
    let parent = $(that).closest(".access-details");
    let selected = $(that).val();

    $(parent).find(".opt-container").not(".document-access-container").not(".access-type-container").not(".access-class-container").remove();

    let folderAccessContainer = `<div class="folder-access-container opt-container">
                                <label class="access-title">Folder Access :</label>
                                <div class="ml-4" style="display:flex;">
                                <div class="mr-4 d-flex align-items-center">
                                <input id="rdbNotPerDepartment" class="prvDocIsPerDepartment" onchange='FolderAccessHandler(this)' 
                                type="radio" name="folder-type" value="ALL"> All
                                </div>
                                <div class="d-flex align-items-center">
                                <input id="rdbPerDepartment" class="prvDocIsPerDepartment" onchange='FolderAccessHandler(this)'
                                type="radio" value="SPECIFIC" name="folder-type"> Specific
                                </div>
                                </div>
                                </div>`;



    if (selected == "DepartmentFolder" || selected == "DepartmentFolderStore") {
        $(parent).append(folderAccessContainer);
    } else if (selected == "Store") {
        $(parent).append(GenerateStoreContainer());
        $("select").select2({ width: '100%' });
    }
}

function GenerateStoreContainer() {
    let storeContainer = `<div class="store-container opt-container d-flex justify-content-center">
                            <div class="col-12">
                            <label class="access-title">Select Store :</label>
                            <select class="form-control form-control-sm select-store">
                            ${GenerateOptions(storeList, "Id", "Name", "Select Store")}
                            </select>
                            <div class='row justify-content-between'>
                            <div class='col-10 store-list'>
                            <ul class='ml-2'>
                            </ul>
                            </div>
                            <div class='text-right'>
                            <button style='margin-right:10px;' type='button'
                            class='btn btn-sm btn-primary mt-2'
                            onclick='AddStore(this)'>Add</button>
                            </div>
                            </div>
                            </div>
                            </div>`

    return storeContainer;
}

function ParentFolderHandler(that) {
    let parent = $(that).closest(".access-details");
    let selected = $(that).val();
    let childFolderElmnt = $(parent).find(".child-folder");

    $(childFolderElmnt).html("");

    if (selected != "") {
        let childFolders = FilterJson(directoryList, "ParentId", selected);

        $(childFolderElmnt).html(GenerateOptions(childFolders, "Id", "Foldername", "Select Child Folder"))
    }
}

function FolderAccessHandler(that) {
    let parent = $(that).closest(".access-details");
    let selected = $(parent).find('input:radio[name=folder-type]:checked').val();
    let selectedAccessType = $(parent).find(".access-type").val();

    let parentFolders = FilterJson(directoryList, ["ParentId"], null);

    let folderContainer = `<div class="folder-container opt-container">
                            <label class="access-title">Folder :</label>
                            <div class="row">
                            <div class="col-lg-6 col-md-12">
                            <label>Parent Folder:</label>
                            <select onchange='ParentFolderHandler(this)' class="form-control form-control-sm parent-folder">
                            ${GenerateOptions(parentFolders, "Id", "Foldername", "Select Parent Folder")}
                            </select>
                            </div>
                            <div class="col-lg-6 col-md-12 mt-lg-0 mt-md-3">
                            <label>Child Folder:</label>
                            <select class="form-control child-folder form-control-sm">
                            <option value="" disabled hidden selected>Select Child Folder</option>
                            </select>
                            </div>
                            </div>
                            <div class='row justify-content-between'>
                            <div class='col-8 folder-list'>
                            <ul class='ml-2'>
                            </ul>
                            </div>
                            <div class='text-right mt-2'>
                            <button type='button' onclick='AddFolder(this)' class='btn btn-primary btn-sm'
                            style='margin-right:10px;'>Add</button>
                            </div>
                            </div>
                            </div>`

    $(parent).find(".opt-container").not(".document-access-container").not(".document-access-container").not(".folder-access-container").not(".access-type-container").not(".access-class-container").remove();

    if (selected == "SPECIFIC" && selectedAccessType == "DepartmentFolder") {
        $(parent).append(folderContainer);
    } else if (selected == "ALL" && selectedAccessType == "DepartmentFolderStore") {
        $(parent).append(GenerateStoreContainer());
    } else if (selected == "SPECIFIC" && selectedAccessType == "DepartmentFolderStore") {
        $(parent).append(folderContainer + GenerateStoreContainer());
    }

    $("select").select2({ width: '100%' });
}

function AddStore(that) {
    let parent = $(that).closest(".access-details");

    let container = $(parent).find(".store-list");

    let store = $(parent).find(".select-store").val();

    if (store == "" || !store) {
        Swal.fire({
            icon: "error",
            title: "Oops",
            text: "Please select the store first!"
        })

        return false;
    }

    if ($(parent).find("li[store='" + store + "']").length > 0) {
        Swal.fire({
            icon: "error",
            title: "Oops",
            text: "Selected store already exists for this set!"
        })

        return false;
    }

    let txtStore = $(parent).find(".select-store option:selected").text();

    $(container).append(`
        <li store='${store}'>${txtStore}
        <span onclick='DeleteListItem(this)' class='fa fa-trash text-danger d-inline-block ml-2'> </span>
        </li>
    `)
}

function AddFolder(that) {
    let parent = $(that).closest(".access-details");

    let container = $(parent).find(".folder-list");

    let parentFolder = $(parent).find(".parent-folder").val();
    let childFolder = $(parent).find(".child-folder").val();

    if (parentFolder == "" || !parentFolder) {
        Swal.fire({
            icon: "error",
            title: "Oops",
            text: "Please choose the parent and child folder first!"
        })

        return false;
    }

    if (childFolder == "" || !childFolder) {
        Swal.fire({
            icon: "error",
            title: "Oops",
            text: "Please choose the child folder first!"
        })

        return false;
    }

    if ($(parent).find("li[childFolder='" + childFolder + "']").length > 0) {
        Swal.fire({
            icon: "error",
            title: "Oops",
            text: "Selected folder already exists for this set!"
        })

        return false;
    }

    let txtParentFolder = $(parent).find(".parent-folder option:selected").text();
    let txtChildFolder = $(parent).find(".child-folder option:selected").text();

    $(container).append(`
        <li parentFolder='${parentFolder}' childFolder='${childFolder}'>${txtParentFolder} - ${txtChildFolder}
        <span onclick='DeleteListItem(this)' class='fa fa-trash text-danger d-inline-block ml-2'> </span>
        </li> 
    `)
}

function DeleteListItem(that) {
    $(that).closest("li").remove();
}

function LoadPDFAsImage(onRender) {
    $("#page-config-area").removeClass("d-none").addClass("d-flex");
    onRender = onRender || function () { };

    currentFile.promise.then(function (pdf) {
        pageDetails.totalPages = pdf.numPages;
        pdf.getPage(pageDetails.currentPage).then(function (page) {
            var scale = 1;
            var viewport = page.getViewport({ scale: scale, });
            //
            // Prepare canvas using PDF page dimensions
            //
            var canvas = document.getElementById('canvas-file');
            var context = canvas.getContext('2d');
            canvas.height = viewport.height;
            canvas.width = viewport.width;
            canvasDetails.height = viewport.height;
            canvasDetails.width = viewport.width;

            //
            // Render PDF page into canvas context
            //
            var renderContext = {
                canvasContext: context,
                viewport: viewport,
            };

            var renderTask = page.render(renderContext);

            renderTask.promise.then(function () {
                onRender();
            })

            $("#lblPageDetails").html(pageDetails.currentPage + " of " + pageDetails.totalPages);
        });
    });
}

function RedrawRectangle(options) {
    options = options || {};
    options.exemption = options.exemption || [];

    var canvasContext = document.getElementById('canvas-file').getContext('2d');

    $.each(currentTemplate, function (i, item) {
        if (options.exemption.indexOf(item.ordinal) == - 1 && item.XCoordinate > 0) {
            DrawRectangleOnCanvas(canvasContext, item.Height, item.Width, item.XCoordinate, item.YCoordinate, item.Page, item.FieldName)
        }
    });
}

function DeleteBatch(id) {
    $.each(jobStatusDetailsList, function (i, item) {
        if (item.jobId == id) {
            jobStatusDetailsList.splice(i, 1);
        }
    })
}

function LoadAccessSetting(ordinal) {
    let settingElmnt = `<div class="access-details" ordinal=${ordinal}>
                                    <div class="title">Set ${ordinal}</div>

                                    <div class="document-access-container opt-container">
                                        <label class="access-title">Document Access :</label>
                                        <div class="ml-4" style="display:flex;">
                                            <div class="mr-4 d-flex align-items-center">
                                                <input name="rdbIsDownloadable" type="radio" checked="checked" value="true"> Downloadable
                                            </div>
                                            <div class="d-flex align-items-center">
                                                <input name="rdbIsDownloadable" type="radio" value="false"> View Only
                                            </div>
                                        </div>
                                    </div>

                                    <div class="access-class-container opt-container">
                                        <label class="access-title">Document Class : </label>
                                        <select onchange="AccessClassHandler(this)" class="form-control form-control-sm access-class">
                                            <option value="" disabled hidden selected>Select Class:</option>
                                            <option value="PUBLIC">Public</option>
                                            <option value="PRIVATE">Private</option>
                                            <option value="RESTRICTED">Restricted</option>
                                            <option value="CONFIDENTIAL">Confidential</option>
                                        </select>
                                    </div>
                                </div>`



    $("#access_setup").append(settingElmnt);
    $("select").select2({ width: '100%' });
}

function DeleteFile(filename) {
    Swal.fire({
        title: 'Are you sure?',
        text: "Are you sure that you want to delete the file " + filename + "?",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            $.each(jobStatusDetailsList, function (i, job) {
                $.each(job.OCRResult, function (i2, result) {
                    if (result.Filename == filename) {
                        jobStatusDetailsList[i].OCRResult.splice(i2, 1);
                        UpdateJob(job.jobId, "OCRResultCount", -1)
                        UpdateJob(job.jobId, "scannedFile", -1);
                    }
                });
            })

            RefreshOCRTable();

            Swal.fire(
                'Deleted!',
                'Your file has been deleted.',
                'success'
            )
        }
    })
}

function EditFileContent(filename, content) {
    $.each(jobStatusDetailsList, function (i, job) {
        $.each(job.OCRResult, function (i2, result) {
            if (result.Filename == filename) {
                jobStatusDetailsList[i].OCRResult[i2].Content = content;
            }
        })
    })
}


function Publish() {
    let accesses = [];
    let isValid = true;

    if (jobStatusDetailsList.length == 0) {
        Swal.fire({
            title: "Oops",
            text: "Please scan files first!",
            icon: "error"
        })

        return false;
    } else {
        let hasFiles = false;

        //track if there is still a background scanning running which needs to be stopped
        let hasInProgress = false;

        $.each(jobStatusDetailsList, function (i, item) {
            if (item.OCRResult.length > 0) {
                hasFiles = true
            }

            if (item.Status == "IN PROGRESS") {
                hasInProgress = true;
            }
        })

        if (hasFiles == false) {
            Swal.fire({
                title: "Oops",
                text: "Please scan files first!",
                icon: "error"
            })

            return false;
        }

        if (hasInProgress == true) {
            Swal.fire({
                title: "Oops",
                text: "Please stop scanning running in background first before publishing!",
                icon: "error"
            })

            return false;
        }
    }


    $(".access-details").each(function (i, item) {
        let name = $(this).find(".title").text();
        let ordinal = $(this).attr("ordinal");
        let result = {};

        let isDownloadable = $(this).find("input[name='rdbIsDownloadable']:checked").val();

        result.IsDownloadable = (isDownloadable == "true");

        let DocumentClass = $(this).find(".access-class").val();

        if (DocumentClass == "" || !DocumentClass) {
            Swal.fire({
                icon: "error",
                title: "Oops",
                text: "Please set the access option in " + name + "!"
            })

            isValid = false;
            return false;
        }

        if (DocumentClass == "PUBLIC") {
            let publicAccessArr = [];

            $(this).find(".public-access-container input:checked").each(function (i2, publicAccess) {
                publicAccessArr.push($(this).val())
            })

            if (publicAccessArr.length == 0) {
                Swal.fire({
                    icon: "error",
                    title: "Oops",
                    text: "Please choose at least one public access option in " + name + "!"
                })

                isValid = false;
                return false;
            }

            result.DocumentClass = DocumentClass
            result.PublicAccess = publicAccessArr
        }
        else if (DocumentClass == "PRIVATE") {
            let accessType = $(this).find(".access-type").val();

            if (accessType == "" || !accessType) {
                Swal.fire({
                    icon: "error",
                    title: "Oops",
                    text: "Please select the access type in " + name + "!"
                })

                isValid = false;
                return false;
            }
            else if (accessType == "DepartmentFolder") {
                let folderAccess = $(this).find(".folder-access-container input:checked").val();

                if (!folderAccess) {
                    Swal.fire({
                        icon: "error",
                        title: "Oops",
                        text: "Please choose folder access option in " + name + "!"
                    })

                    isValid = false;
                    return false;
                }
                else if (folderAccess == "SPECIFIC") {
                    let folders = [];

                    $(this).find(".folder-list li").each(function () {
                        folders.push({
                            ParentFolder: parseInt($(this).attr("parentFolder")),
                            ChildFolder: parseInt($(this).attr("childFolder"))
                        })
                    })

                    if (folders.length == 0) {
                        Swal.fire({
                            icon: "error",
                            title: "Oops",
                            text: "Please set at least one folder in " + name + "!"
                        })

                        isValid = false;
                        return false;
                    }

                    result.Folders = folders;

                }

                result.FolderAccess = folderAccess;
            }
            else if (accessType == "Store") {
                let stores = [];

                $(this).find(".store-list li").each(function () {
                    stores.push(parseInt($(this).attr("store")));
                })

                if (stores.length == 0) {
                    Swal.fire({
                        icon: "error",
                        title: "Oops",
                        text: "Please set at least one store in " + name + "!"
                    })

                    isValid = false;
                    return false;
                }

                result.Stores = stores;
            }
            else if (accessType == "DepartmentFolderStore") {
                let folderAccess = $(this).find(".folder-access-container input:checked").val();

                if (folderAccess == "" || !folderAccess) {
                    Swal.fire({
                        icon: "error",
                        title: "Oops",
                        text: "Please choose the folder access option in " + name + "!"
                    })
                }
                else if (folderAccess == "ALL") {
                    let stores = [];

                    $(this).find(".store-list li").each(function () {
                        stores.push(parseInt($(this).attr("store")))
                    })

                    if (stores.length == 0) {
                        Swal.fire({
                            icon: "error",
                            title: "Oops",
                            text: "Please set at least one store in " + name + "!"
                        })

                        isValid = false;
                        return false;
                    }

                    result.Stores = stores;
                }
                else if (folderAccess == "SPECIFIC") {
                    let folders = [];

                    $(this).find(".folder-list li").each(function () {
                        folders.push({
                            ParentFolder: parseInt($(this).attr("parentFolder")),
                            ChildFolder: parseInt($(this).attr("childFolder"))
                        })
                    })

                    if (folders.length == 0) {
                        Swal.fire({
                            icon: "error",
                            title: "Oops",
                            text: "Please set at least one folder in " + name + "!"
                        })

                        isValid = false;
                        return false;
                    }

                    result.Folders = folders;

                    let stores = [];

                    $(this).find(".store-list li").each(function () {
                        stores.push(parseInt($(this).attr("store")))
                    })

                    if (stores.length == 0) {
                        Swal.fire({
                            icon: "error",
                            title: "Oops",
                            text: "Please set at least one store in " + name + "!"
                        })

                        isValid = false;
                        return false;
                    }

                    result.Stores = stores;
                }

                result.FolderAccess = folderAccess
            }

            result.Accesstype = accessType;
        }
        else if (DocumentClass == "RESTRICTED") {
            let department = parseInt($(this).find(".select-department").val());

            if (department == "" || !department) {
                Swal.fire({
                    icon: "error",
                    title: "Oops",
                    text: "Please select department option in " + name + "!"
                })

                isValid = false;
                return false;
            }

            result.Department = department;
        }

        result.DocumentClass = DocumentClass;
        result.Ordinal = ordinal;
        accesses.push(result);
    });

    if (isValid) {
        let postModels = [];

        $.each(jobStatusDetailsList, function (i, item) {
            if (item.OCRResult.length > 0) {
                let access = GetRowFromJsonByValue(accesses, "Ordinal", item.ordinal);
                
                model = {
                    CreatedBy: {
                        Fullname: userDetails.Fullname,
                        Email: userDetails.Email
                    },
                    OCRResult: item.OCRResult,
                    JobId: item.jobId,
                    ParentId: parseInt(item.template.DocumentType.DefaultDirectoryId),
                    Access: access
                };

                postModels.push(model)
            }
        })

        console.log(JSON.stringify(postModels));

        $.ajax({
            url: "https://190.100.25.71:44363/api/integration/postdata",
            dataType: "JSON",
            contentType: "application/json; charset=utf-8",
            type: "POST",
            data: JSON.stringify(postModels),
            success: function (data) {
                console.log(data);
            }
        })
    }
}

window.onbeforeunload = function (evt) {
    if (jobStatusDetailsList.length > 0) {
        var message = 'Are you sure that you want to leave the site now? The changes you made may not be saved.';
        if (typeof evt == 'undefined') {
            evt = window.event;
        }
        if (evt) {
            evt.returnValue = message;
        }

        console.log(evt);

        return message;
    }
}

