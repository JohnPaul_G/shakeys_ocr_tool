﻿var dtId = 0;
var directories;

//would be populated if user is editing the document type, to track if template name already exists in system.
var selectedDocumentType;

$(document).ready(function () {
    //to update the directories
    $.ajax({
        url: "https://dms.shakeys.solutions/api/integration/gtdmstrfldrs/5437264954385422313",
        type: "GET",
        async: false,
        success: function (data) {
            //this is to reformat the data before passing to jstree;
            directories = data;

            Object.keys(directories).forEach(function (key) {
                directories[key].id = directories[key].Id;
                delete directories[key].Id;

                directories[key].parent = directories[key].ParentId;
                delete directories[key].ParentId;

                directories[key].text = directories[key].Foldername;
                delete directories[key].Foldername;

                if (!directories[key].parent) {
                    directories[key].parent = "#";
                }
            });

            $('#directory_container').jstree({
                'core': {
                    'data': directories
                }
            });
            
            $("#directory_container").on(
                "select_node.jstree", function (evt, data) {
                    $("#txtDirectory").attr("idVal", data.node.id);

                    let selected = data.node.text;
                    let path = "DMS:"
                    let parentArr = uiGetParents(data);

                    for (let i = 0; i < parentArr.length; i++) {
                        path += ("\\" + parentArr[i]);
                    }

                    $("#txtDirectory").val(path + "\\" + selected);
                }
            );
        }
    })

    $("#dtDocuType").DataTable({
        ajax: {
            url: "/DocumentType/GetDocumentTypeList",
            dataType: "json",
            dataSrc: ""
        },
        columns: [
            { data: "DocumentTypeName", title: "Document Type" },
            {
                data: null,
                title: "Folder Name",
                render: function (data) {
                    return "<span class='text-left'>" + (GetNodeDetails(data.DefaultDirectoryId)).text + "</span>";
                }
            },
            {
                data: null,
                title: "Action",
                render: function (data) {
                    return "<button class='btn-success btn btn-sm' onclick='EditDocumentType(" + data.Id +
                        ", \"" + data.DocumentTypeName + "\",\""+ data.DefaultDirectoryId +"\")'> View</button>"
                }
            }
        ],
        columnDefs: [
            {
                targets: [1],
                class: "text-left"
            }
        ]
    })

    $("#btnAdd").click(function (e) {
    	e.preventDefault();
        dtId = 0;

        $("#modalAddUpdate .modal-title").html("Add a New Document Type")
        $("#modalAddUpdate").modal();
    })

    $("#btnSave").click(function () {
        let dtName = $("#txtDTName").val();
        let dtDirectory = $("#txtDirectory").attr("idVal");

        if (!dtName) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please insert the document type name!'
            })

            return false;

        } else if (!dtDirectory) {
            Swal.fire({
                icon: 'error',
                title: 'Oops...',
                text: 'Please set the directory!'
            })

            return false;
            
        } else {
            Swal.fire({
                title: 'Are you sure?',
                text: "Do you want to save the document type now?",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, save it!'
            }).then((result) => {
                if (result.value) {
                    SaveDocumentType(dtName, dtDirectory);
                }
            })
        }
    })
})

function EditDocumentType(id, DTName, DirectoryId) {
    selectedDocumentType = DTName;

    $("#directory_container").jstree("close_all");
    $("#directory_container").jstree().deselect_all(true);

    dtId = id;
    $("#txtDTName").val(DTName);
    
    $.jstree.reference('#directory_container').select_node(DirectoryId);

    $("#modalAddUpdate .modal-title").html(DTName);
    $("#modalAddUpdate").modal();

    let nodeDetails = GetNodeDetails(DirectoryId);
    var elmnt = document.getElementById(nodeDetails.parent + "_anchor");

    setTimeout(function () {
        elmnt.scrollIntoView();
    },200)
}

function GetNodeDetails(id) {
    let result;

    $.each(directories, function (i, item) {
        if (item["id"] == id) {
            result = item
        }
    })

    return result;
}

function uiGetParents(loSelectedNode) {
    let result = [];

    try {
        var lnLevel = loSelectedNode.node.parents.length;
        var lsSelectedID = loSelectedNode.node.id;
        var loParent = $("#" + lsSelectedID);
        var lsParents = loSelectedNode.node.text + ' >';
        for (var ln = 0; ln <= lnLevel - 1; ln++) {
            var loParent = loParent.parent().parent();
            if (loParent.children()[1] != undefined) {
                result.push(loParent.children()[1].text);
            }
        }

        return result.reverse();
    }
    catch (err) {
        alert('Error in uiGetParents');
    }
}

function SaveDocumentType(dtName, dtDirectory) {
    if (dtName) {
        HoldOn.open({
            theme: "sk-circle"
        })

        $.ajax({
            url: "/DocumentType/AddUpdateDocumentType",
            type: "POST",
            dataType: "json",
            contentType: "application/json, charset=utf-8",
            data: JSON.stringify({
                dt: {
                    Id: dtId,
                    DocumentTypeName: dtName,
                    DefaultDirectoryId: dtDirectory,
                    HasDocumentTypeNameChanged: (selectedDocumentType != dtName)
                }                
            }),
            success: function (data) {
                HoldOn.close();
                if (data == "SUCCESS") {
                    Swal.fire({
                        icon: 'success',
                        title: 'Success',
                        text: 'Document type is saved successfully!'
                    }).then(function () {
                        window.location.reload();
                    });
                } else if (data == "NAME ALREADY EXISTS") {
                    Swal.fire({
                        icon: "error",
                        title: "Saving Failed",
                        text: "Document type name already exists in the system.s"
                    })
                } else {
                    Swal.fire({
                        icon: 'error',
                        title: 'Saving Failed',
                        text: 'There is an error processing your request.'
                    })
                }
            }
        })
    }
    
}