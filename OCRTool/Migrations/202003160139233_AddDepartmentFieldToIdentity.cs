﻿namespace OCRTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDepartmentFieldToIdentity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "DepartmentId", c => c.String());
            DropColumn("dbo.AspNetUsers", "IsOnLeave");
        }
        
        public override void Down()
        {
            AddColumn("dbo.AspNetUsers", "IsOnLeave", c => c.Boolean());
            DropColumn("dbo.AspNetUsers", "DepartmentId");
        }
    }
}
