﻿namespace OCRTool.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFullnameFieldToIdentity : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AspNetUsers", "Fullname", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.AspNetUsers", "Fullname");
        }
    }
}
