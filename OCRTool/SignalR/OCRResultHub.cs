﻿using Microsoft.AspNet.SignalR;
using OCRTool.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace OCRTool.SignalR
{
    public class OCRResultHub : Hub
    {
        public void Notify(string user, OCRResult result)
        {
            Clients.All.user(result);
            Clients.Group(user).Announce(result);
        }

        public Task Join(string user)
        {
            return Groups.Add(Context.ConnectionId, user);
        }
    }
}