(function ($) {
    "use strict";
    /*==================================================================
    [ Validate ]*/
    var input = $('.validate-input .input100');

    $('.validate-form').on('submit',function(){
        var check = true;

        for(var i=0; i<input.length; i++) {
            if(validate(input[i]) == false){
                showValidate(input[i]);
                check=false;
            }
        }

        return check;
    });


    $('.validate-form .input100').each(function(){
        $(this).focus(function(){
           hideValidate(this);
        });
    });

    function validate (input) {
        if($(input).attr('type') == 'email' || $(input).attr('name') == 'email') {
            if($(input).val().trim().match(/^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{1,5}|[0-9]{1,3})(\]?)$/) == null) {
                return false;
            }
        }
        else {
            if($(input).val().trim() == ''){
                return false;
            }
        }
    }

    function showValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).addClass('alert-validate');
    }

    function hideValidate(input) {
        var thisAlert = $(input).parent();

        $(thisAlert).removeClass('alert-validate');
    }
    

})(jQuery);

$(document).ready(function () {
    $("#btnSignIn").click(function () {
        Login();
    })

    $("#txtUsername, #txtPassword").keypress(function (e) {
        if (e.which == 13) {
            Login();
        }
    });
})

function Login() {
    let username = $("#txtUsername").val()
    let password = $("#txtPassword").val()

    if (username == "") {
        $("#message").html("Please fill in your username first.")
        return false;
    }

    if (password == "") {
        $("#message").html("Please fill in your password first.")
        return false;
    }

    HoldOn.open({
        theme: "sk-circle"
    });

    console.log("test");

    $.ajax({
        url: "/Account/LoginUser",
        type: "POST",
        dataType: "json",
        contentType: "application/json; charset=utf-8",
        data: JSON.stringify({
            model: {
                Username: username,
                Password: password
            },
            returnUrl: $("#frmLogin").attr("returnUrl")
        }),
        success: function (data) {
            if (data == "INVALID") {
                HoldOn.close()
                $("#message").html("Invalid login attempt, your username or password is incorrect")
            } else {
                window.location.href = data;
            }
        }
    })
}