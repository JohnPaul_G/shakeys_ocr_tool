﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace OCRTool.Models
{
    public class EditUser
    {
        [Required]
        public string Username { get; set; }

        [Required]
        public string UserType { get; set; }

        [Required]
        public string DepartmentId { get; set; }

        [Required]
        public string Fullname { get; set; }
    }
}