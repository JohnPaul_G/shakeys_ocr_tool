﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace OCRTool.Models
{
    public class User
    {
        [Required]
        public string Username { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "Email address provided is not valid.")]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string UserType { get; set; }

        [Required]
        public string DepartmentId { get; set; }

        [Required]
        public string Fullname { get; set; }
    }
}