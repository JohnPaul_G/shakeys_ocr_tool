﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCRTool.Models
{
    public class OCRFileContent
    {
        public string Fieldname { get; set; }
        public string Text { get; set; }
    }
}