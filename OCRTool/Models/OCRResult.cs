﻿using Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace OCRTool.Models
{
    public class OCRResult
    {
        public string Filename { get; set; }
        public string Status { get; set; }
        public Template Template { get; set; }
        public List<OCRFileContent> OCRFileContentList { get; set; }
        public List<TemplateDetails> TemplateDetails { get; set; }
        public string ErrorResult { get; set; }
        public string Type { get; set; }
        public string Id { get; set; }
    }
}