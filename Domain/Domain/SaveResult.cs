﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class SaveResult
    {
        public int Id { get; set; }
        public string Status { get; set; }
    }
}
