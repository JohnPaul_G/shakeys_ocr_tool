﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class SystemLog
    {
        public int Id { get; set; }
        
        public string User { get; set; }
        
        [Required]
        public string Type { get; set; }
        
        public string Description { get; set; }

        public string Status { get; set; }

        [Required]
        public DateTime DTLog { get; set; }
    }
}
