﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class DocProcess
    {
        public int Id { get; set; }

        [Required]
        public string DocProcessName { get; set; }
    }
}
