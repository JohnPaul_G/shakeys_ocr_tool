﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class DocumentType
    {
        public int Id { get; set; }

        [Required]
        public string DocumentTypeName { get; set; }

        //a directory from DMS tru API
        public string DefaultDirectoryId { get; set; }

        [NotMapped]
        public bool HasDocumentTypeNameChanged { get; set; }
    }
}
