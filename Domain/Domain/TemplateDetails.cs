﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class TemplateDetails
    {
        public int Id { get; set; }
        
        [Required]
        public string FieldName { get; set; }

        [Required]
        public int Height { get; set; }

        [Required]
        public int Width { get; set; }

        [Required]
        public int XCoordinate { get; set; }

        [Required]
        public int YCoordinate { get; set; }

        public int? Page { get; set; }

        [ForeignKey("Template")]
        public int TemplateId { get; set; }
        public virtual Template Template { get; set; }
    }
}
