﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class Template
    {
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string TemplateName { get; set; }

        public string CreatedBy { get; set; }

        public int PageWidth { get; set; }

        public int PageHeight { get; set; }

        public DateTime? CreatedOn { get; set; }

        public string FileExtension { get; set; }

        [ForeignKey("DocumentType")]
        public int DocumentTypeId { get; set; }

        public virtual DocumentType DocumentType { get; set; }

        public bool? IsDisabled { get; set; }
        
        //this is to track if template name has changed when updating and check for duplication
        [NotMapped]
        public bool HasTemplateNameChanged { get; set; }
    }
}
