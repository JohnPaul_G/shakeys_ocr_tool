﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class HangfireJob
    {
        public int Id { get; set; }

        [Required]
        public string JobId { get; set; }

        [Required]
        public string User { get; set; }
    }
}
