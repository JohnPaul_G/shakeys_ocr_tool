﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class DocProcessItem
    {
        public int Id { get; set; }

        [ForeignKey("DocProcess")]
        public int DocProcessId { get; set; }
        public virtual DocProcess DocProcess { get; set; }

        [ForeignKey("DocumentType")]
        public int DocumentTypeId { get; set; }
        public virtual DocumentType DocumentType { get; set; }
        
        public int MinQty { get; set; } //Minimum Quantity

        public int Order { get; set; }
    }
}
