﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Domain
{
    public class User
    {
        public string Username { get; set; }

        public string Email { get; set; }

        public string UserType { get; set; }

        public string DepartmentId { get; set; }

        public string Fullname { get; set; }
    }
}
