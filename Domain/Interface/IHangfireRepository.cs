﻿using Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interface
{
    public interface IHangfireRepository
    {
        HangfireJob GetHangfireJob(string jobId);

        List<HangfireJob> GetActiveJobs(string user);

        void AddJob(string jobId, string user);

        void DeleteJob(string jobId);
    }
}
