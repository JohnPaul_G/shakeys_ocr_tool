﻿using Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interface
{
    public interface ITemplateRepository
    {
        List<Template> GetTemplateList(bool includeDisabled = false);

        List<TemplateDetails> GetTemplateDetails(int templateId);

        SaveResult AddUpdateTemplate(Template t, TemplateDetails[] tdArr);

        Template GetTemplateById(int templateId);

        string ChangeTemplateStatus(int templateId, bool willDisable);
    }
}
