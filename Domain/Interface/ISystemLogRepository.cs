﻿using Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interface
{
    public interface ISystemLogRepository
    {
        void AddSystemLog(SystemLog systemLog);

        IEnumerable<SystemLog> GetSystemLogs(DateTime? dateFrom, DateTime? dateTo);
    }
}
