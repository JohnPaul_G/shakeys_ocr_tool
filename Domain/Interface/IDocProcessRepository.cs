﻿using Domain.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Domain.Interface
{
    public interface IDocProcessRepository
    {
        string AddUpdateDocProcess(DocProcess dp, DocProcessItem[] dpiArr);

        List<DocProcess> GetDocProcessList();

        List<DocProcessItem> GetDocProcessItemsList(int docProcessId);
    }
}
