﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class MakeUserNullableSystemLogTable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.SystemLog", "User", c => c.String());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.SystemLog", "User", c => c.String(nullable: false));
        }
    }
}
