﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddIsDisabledFieldToTemplate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Template", "IsDisabled", c => c.Boolean());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Template", "IsDisabled");
        }
    }
}
