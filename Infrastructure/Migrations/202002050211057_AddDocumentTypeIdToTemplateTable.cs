﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDocumentTypeIdToTemplateTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Template", "DocumentTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.Template", "DocumentTypeId");
            AddForeignKey("dbo.Template", "DocumentTypeId", "dbo.DocumentType", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Template", "DocumentTypeId", "dbo.DocumentType");
            DropIndex("dbo.Template", new[] { "DocumentTypeId" });
            DropColumn("dbo.Template", "DocumentTypeId");
        }
    }
}
