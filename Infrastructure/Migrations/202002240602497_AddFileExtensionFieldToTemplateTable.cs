﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFileExtensionFieldToTemplateTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Template", "FileExtension", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Template", "FileExtension");
        }
    }
}
