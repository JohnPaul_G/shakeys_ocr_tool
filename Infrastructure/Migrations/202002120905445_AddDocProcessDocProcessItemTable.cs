﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDocProcessDocProcessItemTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DocProcess",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocProcessName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.DocProcessItem",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        DocProcessId = c.Int(nullable: false),
                        TemplateId = c.Int(nullable: false),
                        MinQty = c.Int(nullable: false),
                        Order = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.DocProcess", t => t.DocProcessId)
                .ForeignKey("dbo.Template", t => t.TemplateId)
                .Index(t => t.DocProcessId)
                .Index(t => t.TemplateId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DocProcessItem", "TemplateId", "dbo.Template");
            DropForeignKey("dbo.DocProcessItem", "DocProcessId", "dbo.DocProcess");
            DropIndex("dbo.DocProcessItem", new[] { "TemplateId" });
            DropIndex("dbo.DocProcessItem", new[] { "DocProcessId" });
            DropTable("dbo.DocProcessItem");
            DropTable("dbo.DocProcess");
        }
    }
}
