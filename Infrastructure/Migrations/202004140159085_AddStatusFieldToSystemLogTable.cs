﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddStatusFieldToSystemLogTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SystemLog", "Status", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.SystemLog", "Status");
        }
    }
}
