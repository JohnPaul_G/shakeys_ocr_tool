﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDefaultDirectoryIdToDocumentType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DocumentType", "DefaultDirectoryId", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.DocumentType", "DefaultDirectoryId");
        }
    }
}
