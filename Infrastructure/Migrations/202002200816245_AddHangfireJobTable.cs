﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddHangfireJobTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.HangfireJob",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    JobId = c.String(),
                })
                .PrimaryKey(t => t.Id);
        }
        
        public override void Down()
        {
            
        }
    }
}
