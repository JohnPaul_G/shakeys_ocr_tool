﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTwoFieldsToTemplateTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Template", "CreatedBy", c => c.String());
            AddColumn("dbo.Template", "CreatedOn", c => c.DateTime());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Template", "CreatedOn");
            DropColumn("dbo.Template", "CreatedBy");
        }
    }
}
