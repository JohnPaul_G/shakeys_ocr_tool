﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedUserFieldToHangfireJob : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.HangfireJob", "User", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.HangfireJob", "User");
        }
    }
}
