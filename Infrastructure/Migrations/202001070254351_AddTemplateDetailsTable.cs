﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTemplateDetailsTable : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TemplateDetails",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FieldName = c.String(nullable: false),
                        Height = c.Int(nullable: false),
                        Width = c.Int(nullable: false),
                        XCoordinate = c.Int(nullable: false),
                        YCoordinate = c.Int(nullable: false),
                        TemplateId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Template", t => t.TemplateId)
                .Index(t => t.TemplateId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.TemplateDetails", "TemplateId", "dbo.Template");
            DropIndex("dbo.TemplateDetails", new[] { "TemplateId" });
            DropTable("dbo.TemplateDetails");
        }
    }
}
