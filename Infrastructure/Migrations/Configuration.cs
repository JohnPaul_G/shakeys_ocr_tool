﻿namespace Infrastructure.Migrations
{
    using Domain.Domain;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Infrastructure.OCRTDBContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Infrastructure.OCRTDBContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method
            //  to avoid creating duplicate seed data.

            //var data = new Template()
            //{
            //    TemplateName = "Sample"
            //};

            //context.Templates.Add(data);
            //context.SaveChanges();
        }
    }
}
