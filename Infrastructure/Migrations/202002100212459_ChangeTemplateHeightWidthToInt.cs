﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangeTemplateHeightWidthToInt : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.Template", "PageWidth", c => c.Int(nullable: false));
            AlterColumn("dbo.Template", "PageHeight", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.Template", "PageHeight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AlterColumn("dbo.Template", "PageWidth", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
    }
}
