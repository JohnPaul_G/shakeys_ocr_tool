﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AllowPageFieldNullable : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.TemplateDetails", "Page", c => c.Int());
        }
        
        public override void Down()
        {
            AlterColumn("dbo.TemplateDetails", "Page", c => c.Int(nullable: false));
        }
    }
}
