﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChangedTemplateIdToDocumentTypeIdInDocProcessTable : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DocProcessItem", "TemplateId", "dbo.Template");
            DropIndex("dbo.DocProcessItem", new[] { "TemplateId" });
            AddColumn("dbo.DocProcessItem", "DocumentTypeId", c => c.Int(nullable: false));
            CreateIndex("dbo.DocProcessItem", "DocumentTypeId");
            AddForeignKey("dbo.DocProcessItem", "DocumentTypeId", "dbo.DocumentType", "Id");
            DropColumn("dbo.DocProcessItem", "TemplateId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.DocProcessItem", "TemplateId", c => c.Int(nullable: false));
            DropForeignKey("dbo.DocProcessItem", "DocumentTypeId", "dbo.DocumentType");
            DropIndex("dbo.DocProcessItem", new[] { "DocumentTypeId" });
            DropColumn("dbo.DocProcessItem", "DocumentTypeId");
            CreateIndex("dbo.DocProcessItem", "TemplateId");
            AddForeignKey("dbo.DocProcessItem", "TemplateId", "dbo.Template", "Id");
        }
    }
}
