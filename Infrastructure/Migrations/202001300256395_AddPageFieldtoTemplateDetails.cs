﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPageFieldtoTemplateDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TemplateDetails", "Page", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.TemplateDetails", "Page");
        }
    }
}
