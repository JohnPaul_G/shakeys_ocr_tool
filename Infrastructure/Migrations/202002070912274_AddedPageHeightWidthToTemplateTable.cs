﻿namespace Infrastructure.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddedPageHeightWidthToTemplateTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Template", "PageWidth", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.Template", "PageHeight", c => c.Decimal(nullable: false, precision: 18, scale: 2));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Template", "PageHeight");
            DropColumn("dbo.Template", "PageWidth");
        }
    }
}
