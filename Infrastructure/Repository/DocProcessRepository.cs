﻿using Domain.Domain;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class DocProcessRepository : IDocProcessRepository
    {
        readonly OCRTDBContext db = new OCRTDBContext();

        public string AddUpdateDocProcess(DocProcess dp, DocProcessItem[] dpiArr)
        {
            try
            {
                if (dp.Id > 0)
                {
                    DocProcess DP = db.DocProcess.FirstOrDefault(x => x.Id == dp.Id);

                    if (DP == null) return "FAILED";

                    DP.DocProcessName = dp.DocProcessName;
                }
                else
                {
                    db.DocProcess.Add(dp);
                }

                db.SaveChanges();

                var oldDpi = db.DocProcessItem.Where(x => x.DocProcessId == dp.Id);
                db.DocProcessItem.RemoveRange(oldDpi);

                foreach (var dpi in dpiArr)
                {
                    dpi.DocProcessId = dp.Id;
                    db.DocProcessItem.Add(dpi);
                }

                db.SaveChanges();

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<DocProcessItem> GetDocProcessItemsList(int docProcessId)
        {
            return db.DocProcessItem.Where(x => x.DocProcessId == docProcessId)
                .OrderBy(x => x.Order).ToList();
        }

        public List<DocProcess> GetDocProcessList()
        {
            return db.DocProcess.ToList();
        }
    }
}
