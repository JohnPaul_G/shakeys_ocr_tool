﻿using Domain.Domain;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class UserRepository : IUserRepository
    {
        readonly OCRTDBContext db = new OCRTDBContext();

        public List<User> GetUserList()
        {
            var sql = new StringBuilder();
            sql.Append("SELECT u.UserName as Username, u.Email, u.DepartmentId, u.Fullname, r.Name as UserType ");
            sql.Append("from AspNetUsers u ");
            sql.Append("LEFT JOIN ");
            sql.Append("AspNetUserRoles ur ");
            sql.Append("ON u.Id = ur.UserId ");
            sql.Append("LEFT JOIN ");
            sql.Append("AspNetRoles r ");
            sql.Append("ON ur.RoleId = r.Id");

            var result = db.Database.SqlQuery<User>(sql.ToString())
                .Select(x => new User {
                    Fullname = x.Fullname ?? "",
                    Email = x.Email,
                    Username = x.Username,
                    UserType = x.UserType ?? "",
                    DepartmentId = x.DepartmentId
                }).ToList();

            return result;
        }
    }
}
