﻿using Domain.Domain;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class HangfireRepository : IHangfireRepository
    {
        OCRTDBContext db = new OCRTDBContext();

        public void AddJob(string jobId, string user)
        {
            var HangfireJob = new HangfireJob
            {
                JobId = jobId,
                User = user
            };

            db.HangfireJob.Add(HangfireJob);

            db.SaveChanges();
        }

        public void DeleteJob(string jobId)
        {
            var rows = db.HangfireJob.Where(x => x.JobId == jobId).ToList();

            db.HangfireJob.RemoveRange(rows);

            db.SaveChanges();
        }

        public List<HangfireJob> GetActiveJobs(string user)
        {
            return db.HangfireJob.Where(x => x.User == user).ToList();
        }

        public HangfireJob GetHangfireJob(string jobId)
        {
            return db.HangfireJob.FirstOrDefault(x => x.JobId == jobId);
        }
    }
}
