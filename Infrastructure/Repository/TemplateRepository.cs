﻿using Domain.Domain;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class TemplateRepository : ITemplateRepository
    {
        OCRTDBContext db = new OCRTDBContext();

        public SaveResult AddUpdateTemplate(Template t, TemplateDetails[] tdArr)
        {
            var result = new SaveResult {
                Status = "SUCCESS"
            };
            
            try
            {
                if (t.Id > 0)
                {
                    if (t.HasTemplateNameChanged && 
                    db.Templates.FirstOrDefault(x => x.TemplateName == t.TemplateName
                    && x.DocumentTypeId == t.DocumentTypeId) != null)
                    {
                        result.Status = "NAME ALREADY EXISTS";
                        return result;
                    }

                    var oldTemplate = db.Templates.FirstOrDefault(x => x.Id == t.Id);

                    if (oldTemplate != null)
                    {
                        oldTemplate.TemplateName = t.TemplateName;
                        oldTemplate.CreatedBy = t.CreatedBy;
                        oldTemplate.DocumentTypeId = t.DocumentTypeId;
                        oldTemplate.PageWidth = t.PageWidth;
                        oldTemplate.PageHeight = t.PageHeight;
                        oldTemplate.FileExtension = t.FileExtension;
                    }

                    var oldTemplateDetails = db.TemplateDetails.Where(x => x.TemplateId == t.Id);
                    db.TemplateDetails.RemoveRange(oldTemplateDetails);
                }
                else
                {
                    if (db.Templates.FirstOrDefault(x => x.TemplateName == t.TemplateName
                    && x.DocumentTypeId == t.DocumentTypeId) != null)
                    {
                        result.Status = "NAME ALREADY EXISTS";
                        return result;   
                    }
                    
                    db.Templates.Add(t);
                }
                
                db.SaveChanges();

                var templateId = t.Id;

                foreach (var td in tdArr)
                {
                    TemplateDetails template_details = new TemplateDetails
                    {
                        TemplateId = templateId,
                        FieldName = td.FieldName,
                        Height = td.Height,
                        Width = td.Width,
                        XCoordinate = td.XCoordinate,
                        YCoordinate = td.YCoordinate,
                        Page = td.Page ?? 1
                    };

                    db.TemplateDetails.Add(template_details);
                }

                db.SaveChanges();

                result.Id = templateId;

                return result;
            }
            catch (Exception ex)
            {
                result.Status = "FAILED";
                return result;
            }
        }

        public List<Template> GetTemplateList(bool includeDisabled = false)
        {
            var templateList = db.Templates.Where(x => 
            (includeDisabled == true ? x.Id > 0 : x.IsDisabled != true))
            .ToList();

            return templateList;
        }

        public List<TemplateDetails> GetTemplateDetails(int templateId)
        {
            var result = db.TemplateDetails.Where(x => x.TemplateId == templateId).ToList();

            return result;
        }

        public Template GetTemplateById(int templateId)
        {
            return db.Templates.FirstOrDefault(x => x.Id == templateId);
        }

        public string ChangeTemplateStatus(int templateId, bool willDisable)
        {
            var template = db.Templates.FirstOrDefault(x => x.Id == templateId);

            if (template == null)
            {
                return "TEMPLATE NOT FOUND";
            }

            template.IsDisabled = willDisable;

            db.SaveChanges();

            return "SUCCESS";
        }
    }
}
