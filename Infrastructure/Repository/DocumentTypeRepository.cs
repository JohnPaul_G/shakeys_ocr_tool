﻿using Domain.Domain;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class DocumentTypeRepository : IDocumentTypeRepository
    {
        OCRTDBContext db = new OCRTDBContext();

        public string AddUpdate(DocumentType dt)
        {
            try
            {
                if (dt.Id > 0)
                {
                    if (dt.HasDocumentTypeNameChanged) {
                        if (db.DocumentTypes.FirstOrDefault(x => x.DocumentTypeName == dt.DocumentTypeName)
                            != null) {
                            return "NAME ALREADY EXISTS";
                        }
                    }

                    DocumentType DT = db.DocumentTypes.FirstOrDefault(x => x.Id == dt.Id);

                    if (DT == null) return null;

                    DT.DocumentTypeName = dt.DocumentTypeName;
                    DT.DefaultDirectoryId = dt.DefaultDirectoryId;
                }
                else
                {
                    if (db.DocumentTypes.FirstOrDefault(x => x.DocumentTypeName == dt.DocumentTypeName)
                            != null)
                    {
                        return "NAME ALREADY EXISTS";
                    }

                    db.DocumentTypes.Add(dt);
                }

                db.SaveChanges();

                return "SUCCESS";
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<DocumentType> GetDocumentTypeList()
        {
            return db.DocumentTypes.ToList();
        }
    }
}
