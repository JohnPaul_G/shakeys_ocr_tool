﻿using Domain.Domain;
using Domain.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure.Repository
{
    public class SystemLogRepository : ISystemLogRepository
    {
        readonly OCRTDBContext db = new OCRTDBContext();

        public void AddSystemLog(SystemLog systemLog)
        {
            db.SystemLog.Add(systemLog);

            db.SaveChanges();
        }

        public IEnumerable<SystemLog> GetSystemLogs(DateTime? dateFrom, DateTime? dateTo)
        {
            var result = db.SystemLog
                .Where(x => x.DTLog >= dateFrom && x.DTLog <= dateTo)
                .ToList();

            return result;
        }
    }
}
