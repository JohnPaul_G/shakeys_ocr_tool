﻿using Domain.Domain;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Infrastructure
{
	public class OCRTDBContext : DbContext
	{
		private const string ConnectionString = "OCRToolConnection";

		public OCRTDBContext()
			: base(ConnectionString)
		{
			var objectContext = (this as IObjectContextAdapter).ObjectContext;
			objectContext.CommandTimeout = 1200;
		}

        #region Dbset

        public DbSet<Template> Templates { get; set; }
        public DbSet<TemplateDetails> TemplateDetails { get; set; }
        public DbSet<DocumentType> DocumentTypes { get; set; }
        public DbSet<DocProcess> DocProcess { get; set; }
        public DbSet<DocProcessItem> DocProcessItem { get; set; }
        public DbSet<HangfireJob> HangfireJob { get; set; }
        public DbSet<SystemLog> SystemLog { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			base.OnModelCreating(modelBuilder);
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
			//modelBuilder.Conventions.Add<CascadeDeleteAttributeConvention>();
		}

		public static OCRTDBContext Create()
		{
			return new OCRTDBContext();
		}
	}
}
